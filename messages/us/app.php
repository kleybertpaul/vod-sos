<?php

return [
    "footerReserved" => "NexTouch @ 2019 All Rights Reserved",
    'Español' => 'Spanish',
    'Ingles' => 'English',
    'Registro' => 'Register',
    'Inicio' => 'Home',
    'Crea tu cuenta' => 'Create your account',
    'Sus datos personales' => 'Your Personal Details',
    'Nombre' => 'First Name',
    'Apellido' => 'Last Name',
    'Correo' => 'Email address',
    'Teléfono' => 'Telephone',
    'Clave' => 'Password',
    'Repetir clave' => 'Re-enter Password',
    'Recordame' => 'Remember Me',
    'SUSCRÍBETE A NUESTRO BOLETÍN' => 'SUBSCRIBE TO OUR NEWSLETTER',
    'Recibe las últimas noticias y actualizaciones' => 'Get Latest News & Update',
    'Ingrese su dirección de correo electrónico ....' => 'Enter Your Email Address....',
    'Suscríbase ahora' => 'Subscribe now',
    'Es un hecho establecido hace mucho tiempo que un lector se distraerá con el contenido legible de una página al mirar su diseño.' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
    'Menu' => 'Menú',
    'Tienda' => 'Shop',
    'Sobre nosotros' => ' About Us',
    'Abrevación' => 'Abbreviation',
    'Actualizar {modelClass} ' => 'To update',
    'Artículos Relacionados' => '
Related Posts',
    'Artículos más vistos' => 'Most Viewed Articles',
    'Cod  Lenguaje' => '
Cod Language',
    'Codigo de verificación' => 'Verification code',
    'Consulte a nuestros especialistas' => 'Consult our specialists
',
    'Contáctenos' => 'Contact Us',
    'Quienes somos' => 'Who We Are',
    'aboutText' => 'Repairing your phone and tablet is what we do best',
    '
NEXTOUCHONLINE.COM es una compañía que repara teléfonos celulares y tabletas a un costo muy bajo. Estamos orgullosos de tratar cada reparación como si el teléfono fuera nuestro.
Ofrecemos a nuestros clientes una solución completa para que sus dispositivos celulares y tabletas defectuosos vuelvan a funcionar correctamente. Reparar su teléfono puede ser muy costoso a través de su operador de telefonía celular, a menudo le deja solo con la opción de comprar un teléfono nuevo. Si no es elegible para la actualización, esta puede ser una opción muy costosa.

<br> <br> <br>
Estamos comprometidos a ayudarlo a extender la vida útil de sus productos al ofrecer la mejor solución para restaurar el dispositivo a su estado original. Al final, lo que es importante para usted es importante para nosotros, y si necesita reemplazar la pantalla LCD rota o si instalamos un nuevo teclado, NEXTOUCHONLINE.COM lo ayudará a encontrar la solución adecuada de manera rápida y conveniente.
<br>
<br>

<strong> Sede corporativa: </strong> 5701, SUNSET DR UNIT 219, SOAMH MIAMI, FL 33143 | PH: 305-6673774

<br>
<br>
<strong> Servicio al cliente </strong>
<br>
Nuestra misión es asegurarnos de que encuentre la reparación exacta que necesita. Si decide visitarnos, encontrará representantes de servicio al cliente de primera categoría que están esperando para ayudarlo. Nuestros expertos en telefonía y tabletas son expertos, experimentados y dedicados a garantizar que su experiencia de reparación sea placentera.
<br> <br> <br>
<strong> Entrega rápida a nivel nacional </strong>
<br>
Procesamos los pedidos de forma rápida y precisa, y enviamos la mayoría de nuestros teléfonos reparados en un día hábil. Si tiene prisa por volver a poner su teléfono en buenas condiciones, ha venido al lugar correcto.' => 'NEXTOUCHONLINE.COM is a company that repairs cellular phones and tablets at a very low cost.  We take great pride in treating every repair as if the phone was our own.
We provide our customers with a complete solution to getting your faulty cellular and tablets device back into working order.  It can be very expensive to repair your phone via your cell phone carrier, often leaving you with only the option to purchase a new phone.  If you are not eligible for upgrade this can be a very expensive option.

<br><br>
We are committed to helping you extend the life of your products by delivering the best solution to restore the device back to their original condition. In the end what’s important to you is important to us, and whether you need to replace the broken LCD, or for us to install a new keypad, NEXTOUCHONLINE.COM will help you find the right solution quickly and conveniently.
<br>
<br>

<strong>Corporate Headquarters:</strong>  2930 NW 72 Ave. Miami, FL 3312, +1 (786) 6160509

<br>
<br>
<strong>Customer Service</strong>
<br>
It is our mission to make sure you find the exact repair you need.  If you decide to visit us you will find top-notch customer service representatives who are waiting to assist you. Our phone and tablets experts are knowledgeable, experienced, and dedicated to making sure your repair experience is a pleasant one.
<br><br>
<strong>Fast Nation-wide Delivery</strong>
<br>
We process orders quickly, accurately, and ship most of our repaired phones back within one business day. If you’re in a hurry to get your phone back in working condition you have come to the right place.',
    'Tel' => 'Tel',
    'Enviar' => 'Submit',
    'Correo' => 'Mail',
    'Horario' => 'Open',
    'Lunes a sábado de 9:00 a 18:00.' => 'Mon – Sat: 9:00 – 18:00',
    '¡Deja un mensaje!' => 'Leave a message!',
    'Nombre ' => 'Name',
    'Sitio web' => 'Website',
    'Preguntas más frecuentes' => 'Faq',
    'Preguntas frecuentes' => 'Frequently Asked Questions',
    'Nombre' => 'First name',
    'Nombre y Apellido' => 'Name and surname',
    '¿Qué tipo de opciones de pago aceptan?' => 'What type of payment options do you accept?',
    'País' => 'country',
     'Si' => 'Yes we do.',
     'Política de privacidad' => 'Privacy Policy',
     '
Esta Política de privacidad revela las prácticas de privacidad de este sitio. Específicamente, describe los tipos de información que recopilamos sobre usted mientras utiliza el sitio y las formas en que usamos y compartimos esta información. <br>
<br>

<strong> Recopilación y uso de información personal </strong>
<br>
  Si visita el sitio web, no recopilamos ninguna información de identificación personal sobre usted (como su nombre, dirección, número de teléfono o dirección de correo electrónico) a menos que la proporcione. Puede proporcionar información personal para ponerse en contacto con nosotros o informar un error. Utilizamos la información personal que nos proporciona con el único fin de comunicarnos con usted. No divulgaremos ninguna información personal a un tercero sin su previa autorización expresa por escrito.
<br> <br> <br>
<strong> Archivos de registro </strong>
<br>
NEXTOUCHONLINE.COM recibe y registra automáticamente la información en los registros de nuestro servidor desde su navegador, incluida su dirección IP, la fecha y la hora de acceso, el tipo de navegador, el tipo de plataforma, la URL de referencia y la página que solicita. Esta información no lo identifica personalmente y se utiliza de forma agregada para ayudarnos a mejorar nuestro sitio web y decirnos cuántas personas visitan nuestro sitio durante un período de tiempo.
<br> <br> <br>
<strong> Cookies </strong>
<br>
Una cookie es una pequeña cantidad de datos que se envían a su navegador desde un servidor web y se almacenan en su computadora. Por ejemplo, un sitio web puede usar cookies para almacenar y, a veces, rastrear información sobre usted, sus preferencias o las páginas que visitó por última vez.
<br>
No utilizamos cookies para almacenar o recopilar información en nuestro sitio web. <br>

Sin embargo, algunos de nuestros socios comerciales utilizan cookies en nuestro sitio. Usamos empresas de publicidad de terceros (como Google) para publicar anuncios cuando visita nuestro sitio. Estas compañías pueden usar información (sin incluir su nombre, dirección, dirección de correo electrónico o número de teléfono) sobre sus visitas a este y otros sitios web para proporcionar anuncios sobre bienes y servicios de su interés. Google, como proveedor externo, utiliza cookies para publicar anuncios en nuestro sitio. El uso de Google de la cookie DART le permite publicar anuncios a nuestros usuarios en función de su visita a nuestros sitios y otros sitios en Internet. Los usuarios pueden optar por no usar la cookie de DART visitando la política de privacidad de la red de contenido y anuncios de Google. <br> <br>

<strong> Enlaces a otros sitios </strong>
<br>
Este sitio incluye enlaces a sitios web externos. Animamos a los usuarios a revisar la política de privacidad de cada sitio vinculado, ya que no podemos ser responsables de las prácticas de privacidad de otros sitios. <br> <br>

<strong> Información de contacto </strong>
<br>
Si tiene preguntas o sugerencias sobre nuestra política de privacidad, contáctenos.
Teléfono: 305-6673774' => 'This Privacy Policy discloses the privacy practices of this site. Specifically, it outlines the types of information that we gather about you while you are using the site, and the ways in which we use and share this information.<br>
<br>

<strong>Collection and Use of Personal Information</strong>
<br>
  If you visit the Website, we do not collect any personally identifiable information about you (such as your name, address, phone number or email address) unless you provide it. You may provide personal information in order to contact us or report of an error. We use the personal information you provide us solely for the purpose of communicating back to you. We will not disclose any personal information to a third party without your express prior written authorization.
<br><br>
<strong>Log Files</strong>
<br>
NEXTOUCHONLINE.COM automatically receives and records information on our server logs from your browser, including your IP address, date and time of access, browser type, platform type, referring url, and the page you request. This information does not identify you personally and is used in an aggregate way to help us improve our web site and tell us how many people visit our site over a period of time.
<br><br>
<strong>Cookies</strong>
<br>
A cookie is a small amount of data that is sent to your browser from a web server and stored on your computer. For example, a web site may use cookies to store and sometimes track information about you, your preferences, or the pages you last visited.
<br>
We do not use cookies to store or collect information on our web site.<br>

However, some of our business partners use cookies on our site. We use third-party advertising companies (such as Google) to serve ads when you visit our site. These companies may use information (not including your name, address, email address, or telephone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you. Google, as a third party vendor, uses cookies to serve ads on our site. Google use of the DART cookie enables it to serve ads to our users based on their visit to our sites and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.<br><br>

<strong>Links to Other Sites</strong>
<br>
This site includes links to external web sites. We encourage users to review the privacy policy of each linked site, as we cannot be responsible for the privacy practices of other sites.<br><br>

<strong>Contact Information</strong>
<br>
If you have questions or suggestions about our privacy policy, please contact us .
Phone: 305-6673774',
'warranties' => 'By placing purchase order(s) with NEXTOUCH LLC. you are agreeing to the pricings, payments term, shipping charges, and conditions. We reserve the right at any time after receiving your purchase order(s) to decline the order(s) for any reason. All new customers are required to pay in advance 100% of the 1st initial order; payment may be made with credit card or wire transfer.<br>
<br>

<strong>Shipping & Handling</strong>
<br>
According to our work schedule, usually an order can be shipped on the same day if you have submitted the order by 11 am, but it is not guaranteed. If the order cannot be shipped within the same day, then usually the order will be shipped on the following business day. We have USPS and other shipping companies to provide the best service to fit your business needs and to ensure on-time delivery. It is your responsibility to accept the delivery and any charges that may apply, including any extra charges for services you requested, i.e., special routing, inside delivery, etc.

Shipping charge(s), re-shipping charge(s), and a 30% restocking fee may apply to all rejected or returned package(s)
<br><br>
<strong>Receiving</strong>
<br>
Please inspect the box(es) for signs of damage before opening your product. If you think there may be product damage, please take a photo of the unopened box. This will help prove that damage was the carrier’s fault. Please retain original packaging until you have verified your product performs as expected in accordance with the warranty & policy.

Note: Once your order has been delivered to your location, you have up to 24 hours to report any shortage and wrong products.
<br><br>
<strong>Limited Warranty Information</strong>
<br>
Our products are supported by different limited warranties. This Limited Warranty Information is only for Defective Products. Please read each warranty to determine which terms and conditions apply.

10 days limited warranties applies to all of our products, except for Bluetooth accessories.
10 days limited warranties applies to Bluetooth accessories.

We are not responsible for any misused, abused, or any damage caused by human error on installation. For more information please call our Sales Department at  305-6673774<br><br>

<strong>Cancellation</strong>
<br>
You are responsible for 30% restocking fee for order that is cancelled after the S.O. is issued & processed. The definition of Processed is when the product requested has been put aside for fulfillment of the order. This will be an automatic response if we receive no notification from you for 7 business days.<br><br>

<strong>Return & Exchange</strong>
<br>
We offer 5 days return or exchange on good products. All return and exchange products must be in its original condition. A credit or exchange may be issued per request upon inspection. Shipping cost is non-refundable. Exchange may be accepted for any defective products. Please refer to Limited Warranty Information and RMA form for details. We do not accept any return on all custom orders unless it is a mishap caused internally. Allcloseout/clearance products are final sale; no exchange, no refund, and no return.',
'Política de compra' => 'Purchase Policy',
'Buscar en toda la tienda aquí ...' => 'Search entire store here...',
'Categorías' => 'Categories',
'address' => 'Contact',
'información' => 'Information',
'Hacer una cita' => 'Make an Appointment',
'My account' => 'My account',
'Iniciar sesión' => 'Sign In',
'Ver carrito' => 'View Cart',
'Rastrear mi orden' => 'Track My Order',
'garantías' => 'Warranties',
'franquicia' => 'Franchise',
'Soporte' => 'Support',
'Síguenos en:' => 'Follow us on :',
'Recuerdame' => 'Remember Me',
'Ya tienes una cuenta con nosotros.' => 'Already have an account with us',
'Inicie sesión aquí' => 'Login Here',
'¿Olvidaste tu contraseña?' => 'Forgot your password?',
'¿Nuevo en Nextouch?' => 'New to Nextouch?',
'Crear nueva cuenta' => 'Create New Account',
'Inicio de sesión del cliente' => 'Customer Login',
'Cerrar sesion' => 'Sign off',
'Todas las categorias' => 'All Categories',
'productos de interes' => 'products of interest',
'Producto' => 'Product',
'Nombre producto' => 'Product Name',
'Precio' => 'Price',
'Cantidad' => 'Quantity',
'Acción' => 'Action',
'Exito!' => 'Success!',
'El producto se ha añadido a su lista.' => 'Product have added to your list.',
'Seguir comprando' => 'Continue Shopping',
'Carrito total' => 'Cart Total',
'Artículo (s) Subtotal' => 'Item(s) Subtotal',
'Envío' => 'Shipping',
'Cantidad pagable' => 'Amount Payable',
'Pasar por la caja' => 'Proceed to checkout',
'$' => '$',
'Procesar' => 'Checkout',
'Buscar' => 'Search',
'Carrito Subtotal' => 'Cart Subtotal',
'Comprar' => 'Shopping',
'Agregar al carrito' => 'Add to Cart',
'Descripción' => 'Description',
'Disponibilidad' => 'Availability',
'Cuenta' => 'account',


'Your transaction is Approved' => 'Your transaction is Approved',
'We will contact you' => 'We will contact you',
'We are verifying the transaction' => 'We are verifying the transaction',
'Your transaction is in process' => 'Your transaction is in process',
'message_init_account' => 'Welcome to your account, here you will see the orders and the details of your requested products',

'web_title' => 'NexTouch - Smartphone & tablets accessories and parts',
'metaKeywords' => 'NexTouch - Smartphone, tablets, accessories and parts ',
'metaDescription' => 'NexTouch - Accessories - Parts, for mobiles and tablets, products, phones, mobiles, Repairing your phone and tablet is what we do best. Company that repairs cell phones and tablets at very low cost. We are proud to treat each of our services. We offer our customers a complete solution for their defective cell phones and tablets to work properly again. ',
'metaImage' => 'https://nextouchonline.com/themes/nexttouch/img/logo/logoingles.png',
'metaAutor' => 'NexTouch',
'metaUrl' => 'https://nextouchonline.com/',
'metaSite' => '@nextouchonline',
'metaCreator' => '@nextouchonline',

'metaTitle_shop' => 'Shop - NexTouch - Smartphone & tablets accessories and parts',
'metaKeywords_shop' => 'Shop - NexTouch - accessories - Smartphone, tablets, accessories and parts',
'metaDescription_shop' => 'Shop - ',

'metaTitle_about' => 'About - NexTouch - Smartphone & tablets accessories and parts',
'metaKeywords_about' => 'About - NexTouch - accessories - Smartphone, tablets, accessories and parts',
'metaDescription_about' => 'About - ',

'metaTitle_contact' => 'Contact - NexTouch - Smartphone & tablets accessories and parts',
'metaKeywords_contact' => 'Contact - NexTouch - accessories - Smartphone, tablets, accessories and parts',
'metaDescription_contact' => 'Contact - ',

'metaTitle_faq' => 'Frequently asked questions - NexTouch - Smartphone & tablets accessories and parts',
'metaKeywords_faq' => 'Frequently asked questions - NexTouch - accessories - Smartphone, tablets, accessories and parts',
'metaDescription_faq' => 'Frequently asked questions - ',

'metaTitle_policy' => 'Privacy Policy - NexTouch - Smartphone & tablets accessories and parts',
'metaKeywords_policy' => 'Privacy Policy - NexTouch - accessories - Smartphone, tablets, accessories and parts',
'metaDescription_policy' => 'Privacy Policy - ',

'metaTitle_warranties' => 'Warranties - NexTouch - Smartphone & tablets accessories and parts',
'metaKeywords_warranties' => 'Warranties - NexTouch - accessories - Smartphone, tablets, accessories and parts',
'metaDescription_warranties' => 'Warranties - ',
'aboutDES' => 'NEXTOUCHONLINE.COM is a company that repairs cellular phones and tablets at a very low cost.  We take great pride in treating every repair as if the phone was our own.
We provide our customers with a complete solution to getting your faulty cellular and tablets device back into working order.  It can be very expensive to repair your phone via your cell phone carrier, often leaving you with only the option to purchase a new phone.  If you are not eligible for upgrade this can be a very expensive option.

<br><br>
We are committed to helping you extend the life of your products by delivering the best solution to restore the device back to their original condition. In the end what’s important to you is important to us, and whether you need to replace the broken LCD, or for us to install a new keypad, NEXTOUCHONLINE.COM will help you find the right solution quickly and conveniently.
<br>
<br>

<strong>Corporate Headquarters:</strong>  2930 NW 72 Ave. Miami, FL 3312, +1 (786) 6160509

<br>
<br>
<strong>Customer Service</strong>
<br>
It is our mission to make sure you find the exact repair you need.  If you decide to visit us you will find top-notch customer service representatives who are waiting to assist you. Our phone and tablets experts are knowledgeable, experienced, and dedicated to making sure your repair experience is a pleasant one.
<br><br>
<strong>Fast Nation-wide Delivery</strong>
<br>
We process orders quickly, accurately, and ship most of our repaired phones back within one business day. If you’re in a hurry to get your phone back in working condition you have come to the right place.' ,
'ES' => 'Spain',
'VE' => 'Vezuela',
'US' => 'U.S',
'seleccione' => 'Select country',
'New Arrivals' => 'New Arrivals',
'carprod' => 'Product have added to your wishlist.',
'RegisExitoso' => 'Successful registration in NEXTOUCH',
'comunidad' => 'you belong to the community of NEXTOUCH',
'solicitudExi' => 'Account request created',
'visitenos' => 'Visit us at https://nextouchonline.com/',
'felicidades' => 'Congratulations',
'Scroll-top' => 'Top',
'formulario' => 'Send us a message through the contact form',
'clienteNuevo' => 'Are you a new client? Start here',
'mensajeContact' =>'New contact message from NEXTOUCH',
'mensajeExitoso' => 'Message sent correctly, we will respond soon.',
'ActAprobado' => 'Approved',
'ActRechazado' => 'Rejected',
'textActRechazado' => 'Are you sure to update the status to rejected?',
'textActAprobado' => 'Are you sure of updating the status to approved?',
'estatusPedidoAprobado' =>  'The order has been approved by NEXTOUCHONLINE.COM',
'estatusPedidoRechazado' =>  'The order has been rejected by NEXTOUCHONLINE.COM',
'tipoOrdenAprobada' => 'Approved the order with the number:',
'tipoOrdenRechazada' => 'Rejected the order with the number:',
'informacionAprobada' => 'Soon we will send your product (s)',
'informacionRechazada' => 'For more information contact us from our website https://nextouchonline.com',
'ActEnviado' => 'Sent',
'textActEnviado' => 'Are you sure to update the status sent?',
'estatusPedidoEnviado' =>  'The order has been sent by NEXTOUCHONLINE.COM',
'tipoOrdenEnviado' => 'Sent The order with the number:',
'informacionEnviado' => 'Product (s) sent from NEXTOUCHONLINE.COM',
'correInvalido' => 'Invalid email. Please enter your email again.',
'recuperarContrasena' => 'Please enter your email to retrieve password',
'ingreseCorreo' => 'Enter your email',
'enviar' => 'Submit',
'cambioContrasena' => 'CHANGE OF PASSWORD APPROVED FROM NEXTTOUCH',
'codigoRecuperacion' => 'Recovery code:',
'ingreseCodigoText' => 'Please enter the code that was sent to your email',
'ingreseCodigo' => 'Enter your code',
'claveInvalido' => 'invalid code Please enter your code again.',
'ingreseContrasenaNueva' => 'Enter your new password',
'ingreseContrasenaNuevaText' => 'Please enter your new password greater than 8 digits:',
'ingreseNuevaClave' => 'Enter your new password',
'repitaNuevaClave' => 'Repeat your new password',
'claveMayor' => 'Password must be equal to or greater than 8 digits',
'claveDiferente' => 'The keys are different',
'cambioExitoso' => 'Successful key change',
'detalle' => 'See detail',
'orders' => 'My Orders',
'accountDas' => 'Account Dashboard',
'hello' => 'hello',
'fechaHora' => 'Date and Time',
'total' => 'Total',
'opciones' => 'Options',
'detalle' => 'See detail',
'details' => 'Details',
'datosPay' => 'PayPal data',
'datosGenerales' => 'General data',
'estadoCompra' => 'Purchase status',
'aprobada' => 'Approved',
'noaprobada' => 'Not approved',
'ordOver' => 'Order Overview',
'product' => 'Product',
'productDetail' => 'Product Detail',
'subTotal' => 'Sub Total',
'cartTotal' => 'Cart Total',
'itemSub' => 'Item(s) Subtotal',
'ship' => 'Shipping',
'amouPay' => 'Amount Payable',




];

