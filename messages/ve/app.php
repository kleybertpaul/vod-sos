<?php

	return array(
		
		"footerReserved" => "NexTouch @ 2019 All Rights Reserved",
		"Spanish" => "Español",
		"English" => "Ingles",
		"Register" => "Registro",
		"Home" => "Inicio",
		"Create your account" => "Crea tu cuenta",
		"Your Personal Details" => "Sus datos personales",
		"First Name" => "Nombre",
		"Last Name" => "Apellido",
		"Email address" => "Correo",
		"Telephone" => "Teléfono",
		"Password" => "Clave",		
		"Re-enter Password" => "Repetir clave", 	
		"username" => "Usuario",
		"password" => "Contraseña",
		"remember me" => "Recordarme",
		"SUBSCRIBE TO OUR NEWSLETTER" => "SUSCRÍBETE A NUESTRO BOLETÍN",
		"Get Latest News & Update" => "Recibe las últimas noticias y actualizaciones",
		"Enter Your Email Address...." => "Ingrese su dirección de correo electrónico ....",	
		"Subscribe now" => "Suscríbase ahora",
		"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout." => "Es un hecho establecido hace mucho tiempo que un lector se distraerá con el contenido legible de una página al mirar su diseño.",	
		"Menú" => "Menu",
		"Shop" => "Tienda",
		"About Us" => "Sobre nosotros",
		'Contact' => 'Contáctenos',
		'Who We Are' => 'Quienes somos',
		'aboutText' => 'Reparar tu teléfono y tableta es lo que mejor hacemos',
		'NEXTOUCHONLINE.COM is a company that repairs cellular phones and tablets at a very low cost.  We take great pride in treating every repair as if the phone was our own.
We provide our customers with a complete solution to getting your faulty cellular and tablets device back into working order.  It can be very expensive to repair your phone via your cell phone carrier, often leaving you with only the option to purchase a new phone.  If you are not eligible for upgrade this can be a very expensive option.

<br><br>
We are committed to helping you extend the life of your products by delivering the best solution to restore the device back to their original condition. In the end what’s important to you is important to us, and whether you need to replace the broken LCD, or for us to install a new keypad, NEXTOUCHONLINE.COM will help you find the right solution quickly and conveniently.
<br>
<br>

<strong>Corporate Headquarters:</strong>  2930 NW 72 Ave. Miami, FL 3312, +1 (786) 6160509

<br>
<br>
<strong>Customer Service</strong>
<br>
It is our mission to make sure you find the exact repair you need.  If you decide to visit us you will find top-notch customer service representatives who are waiting to assist you. Our phone and tablets experts are knowledgeable, experienced, and dedicated to making sure your repair experience is a pleasant one.
<br><br>
<strong>Fast Nation-wide Delivery</strong>
<br>
We process orders quickly, accurately, and ship most of our repaired phones back within one business day. If you’re in a hurry to get your phone back in working condition you have come to the right place.' => 'NEXTOUCHONLINE.COM es una compañía que repara teléfonos celulares y tabletas a un costo muy bajo. Estamos orgullosos de tratar cada reparación como si el teléfono fuera nuestro.
Ofrecemos a nuestros clientes una solución completa para que sus dispositivos celulares y tabletas defectuosos vuelvan a funcionar correctamente. Reparar su teléfono puede ser muy costoso a través de su operador de telefonía celular, a menudo le deja solo con la opción de comprar un teléfono nuevo. Si no es elegible para la actualización, esta puede ser una opción muy costosa.

<br> <br> <br>
Estamos comprometidos a ayudarlo a extender la vida útil de sus productos al ofrecer la mejor solución para restaurar el dispositivo a su estado original. Al final, lo que es importante para usted es importante para nosotros, y si necesita reemplazar la pantalla LCD rota o si instalamos un nuevo teclado, NEXTOUCHONLINE.COM lo ayudará a encontrar la solución adecuada de manera rápida y conveniente.
<br>
<br>

<strong> Sede corporativa: </strong> 5701, SUNSET DR UNIT 219, SOAMH MIAMI, FL 33143 | PH: 305-6673774

<br>
<br>
<strong> Servicio al cliente </strong>
<br>
Nuestra misión es asegurarnos de que encuentre la reparación exacta que necesita. Si decide visitarnos, encontrará representantes de servicio al cliente de primera categoría que están esperando para ayudarlo. Nuestros expertos en telefonía y tabletas son expertos, experimentados y dedicados a garantizar que su experiencia de reparación sea placentera.
<br> <br> <br>
<strong> Entrega rápida a nivel nacional </strong>
<br>
Procesamos los pedidos de forma rápida y precisa, y enviamos la mayoría de nuestros teléfonos reparados en un día hábil. Si tiene prisa por volver a poner su teléfono en buenas condiciones, ha venido al lugar correcto.',
   'Tel' => 'Tel',
    'Submit' => 'Enviar',
    'Mail' => 'Correo',
    'Open' => 'Horario',
    'Mon – Sat: 9:00 – 18:00' => 'Lunes a sábado de 9:00 a 18:00.',
    'Leave a message!' => '¡Deja un mensaje!',
    'Website' => 'Sitio web',
    'Faq' => 'Preguntas más frecuentes',
    'Frequently Asked Questions' => 'Preguntas frecuentes',
    'What type of payment options do you accept?' => '¿Qué tipo de opciones de pago aceptan?',
    'We accept Cash and all major Credit Cards. We DO NOT accept any forms of checks' => 'Aceptamos efectivo y todas las principales tarjetas de crédito. No aceptamos ninguna forma de cheques.',
    'How long does my repair takes?' => '¿Cuánto tiempo dura mi reparación?',
    'iPhone 5 Crack Screen takes..........20-30mins.<br>
iPhone 4/4S Crack Screen takes..........30-45mins.<br>
iPhone 3/3S Crack Screen takes..........30-45mins.<br>
iPhone Water Damage........Same day if dropped off before 2pm. Otherwise next day. <br>
iPad Cracked Screen..........Call to schedule an appointment.' => 'La pantalla de crack del iPhone 5 toma .......... 20-30mins. <br>
La pantalla de crack del iPhone 4 / 4S toma .......... 30-45mins. <br>
La pantalla de crack del iPhone 3 / 3S toma .......... 30-45mins. <br>
Daños por agua en el iPhone ... el mismo día si se deja antes de las 2 p. m. De lo contrario al día siguiente. <br>
Pantalla agrietada del iPad .......... Llame para programar una cita.',
'Will I lose the data on my iPhone/iPad during the repair process?' => '¿Perderé los datos en mi iPhone / iPad durante el proceso de reparación?',
'No! However, for your concerns and the safety of your privacy; you should always back up your data before you bring in your device for repair and you may also lock your device because our technician does not need to access the software side in order to do the repair, we only repair the hardware. (Make sure you remember your own password).' => '
¡No! Sin embargo, por sus preocupaciones y la seguridad de su privacidad; siempre debe hacer una copia de seguridad de sus datos antes de llevar su dispositivo para su reparación y también puede bloquearlo porque nuestro técnico no necesita acceder al lado del software para realizar la reparación, solo reparamos el hardware. (Asegúrese de recordar su propia contraseña).',
 'What kind of guarantee or warranty does your company offer?' => '¿Qué tipo de garantía o garantía ofrece su empresa?',
 'Yes we do.' => 'Si',
  'Can I walk in and surprise you guys without an appointment?' => '¿Puedo entrar y sorprenderlos sin una cita?',
         'I don’t have time to drop it off, what can I do?' => 'No tengo tiempo para dejarlo, ¿qué puedo hacer?',
          'I have a 3G/3GS do you guys repair that also?' => 'Tengo un 3G / 3GS ¿ustedes también reparan eso?',
                  'All iPhone 3G/3GS, 4/4s, 5 Touch Screen are guarantee for 90 days
All iPad Touch Screen are guarantee for 90 days 
(Any future accidental damages or glass break ge will NOT be covered).' => ' Todos los iPhone 3G/3GS, 4/4s, 5 pantallas táctiles tienen una garantía de 90 días
Todas las pantallas táctiles del iPad tienen una garantía de 90 días.
(NO se cubrirá ningún daño accidental futuro o rotura de vidrio).',
      'Yes, of course you can. Just remember to bring your iPhone or iPad and a smile. :)' => 'Sí por supuesto que puedes. Solo recuerda traer tu iPhone o iPad y una sonrisa. :)',
      'We understand that you are a very busy person. So if you live or work in San Francisco Area we can have one of our technician go to you and pick up your device for repair or you can simply mail your device to our office address <strong>Attn: NEXTOUCHREPAIR </strong>Call us now to schedule a pick up time. This service is currently free of charge to you. We guarantee to save you gas, time & money! :) Life is great!' => 'Entendemos que eres una persona muy ocupada. Por lo tanto, si vive o trabaja en el área de San Francisco, podemos pedirle a uno de nuestros técnicos que lo visite y recoja su dispositivo para repararlo o simplemente puede enviarlo por correo a nuestra dirección de la oficina <strong> A la atención de: NEXTOUCHREPAIR </strong> Llámenos ahora para programar una hora de recogida. Este servicio es actualmente gratuito para usted. Garantizamos ahorrarle gas, tiempo y dinero! :) ¡La vida es genial!',
      'Privacy Policy' => 'Política de privacidad',
           'This Privacy Policy discloses the privacy practices of this site. Specifically, it outlines the types of information that we gather about you while you are using the site, and the ways in which we use and share this information.<br>
<br>

<strong>Collection and Use of Personal Information</strong>
<br>
  If you visit the Website, we do not collect any personally identifiable information about you (such as your name, address, phone number or email address) unless you provide it. You may provide personal information in order to contact us or report of an error. We use the personal information you provide us solely for the purpose of communicating back to you. We will not disclose any personal information to a third party without your express prior written authorization.
<br><br>
<strong>Log Files</strong>
<br>
NEXTOUCHONLINE.COM automatically receives and records information on our server logs from your browser, including your IP address, date and time of access, browser type, platform type, referring url, and the page you request. This information does not identify you personally and is used in an aggregate way to help us improve our web site and tell us how many people visit our site over a period of time.
<br><br>
<strong>Cookies</strong>
<br>
A cookie is a small amount of data that is sent to your browser from a web server and stored on your computer. For example, a web site may use cookies to store and sometimes track information about you, your preferences, or the pages you last visited.
<br>
We do not use cookies to store or collect information on our web site.<br>

However, some of our business partners use cookies on our site. We use third-party advertising companies (such as Google) to serve ads when you visit our site. These companies may use information (not including your name, address, email address, or telephone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you. Google, as a third party vendor, uses cookies to serve ads on our site. Google use of the DART cookie enables it to serve ads to our users based on their visit to our sites and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.<br><br>

<strong>Links to Other Sites</strong>
<br>
This site includes links to external web sites. We encourage users to review the privacy policy of each linked site, as we cannot be responsible for the privacy practices of other sites.<br><br>

<strong>Contact Information</strong>
<br>
If you have questions or suggestions about our privacy policy, please contact us .
Phone: 305-6673774' => '
Esta Política de privacidad revela las prácticas de privacidad de este sitio. Específicamente, describe los tipos de información que recopilamos sobre usted mientras utiliza el sitio y las formas en que usamos y compartimos esta información. <br>
<br>

<strong> Recopilación y uso de información personal </strong>
<br>
  Si visita el sitio web, no recopilamos ninguna información de identificación personal sobre usted (como su nombre, dirección, número de teléfono o dirección de correo electrónico) a menos que la proporcione. Puede proporcionar información personal para ponerse en contacto con nosotros o informar un error. Utilizamos la información personal que nos proporciona con el único fin de comunicarnos con usted. No divulgaremos ninguna información personal a un tercero sin su previa autorización expresa por escrito.
<br> <br> <br>
<strong> Archivos de registro </strong>
<br>
NEXTOUCHONLINE.COM recibe y registra automáticamente la información en los registros de nuestro servidor desde su navegador, incluida su dirección IP, la fecha y la hora de acceso, el tipo de navegador, el tipo de plataforma, la URL de referencia y la página que solicita. Esta información no lo identifica personalmente y se utiliza de forma agregada para ayudarnos a mejorar nuestro sitio web y decirnos cuántas personas visitan nuestro sitio durante un período de tiempo.
<br> <br> <br>
<strong> Cookies </strong>
<br>
Una cookie es una pequeña cantidad de datos que se envían a su navegador desde un servidor web y se almacenan en su computadora. Por ejemplo, un sitio web puede usar cookies para almacenar y, a veces, rastrear información sobre usted, sus preferencias o las páginas que visitó por última vez.
<br>
No utilizamos cookies para almacenar o recopilar información en nuestro sitio web. <br>

Sin embargo, algunos de nuestros socios comerciales utilizan cookies en nuestro sitio. Usamos empresas de publicidad de terceros (como Google) para publicar anuncios cuando visita nuestro sitio. Estas compañías pueden usar información (sin incluir su nombre, dirección, dirección de correo electrónico o número de teléfono) sobre sus visitas a este y otros sitios web para proporcionar anuncios sobre bienes y servicios de su interés. Google, como proveedor externo, utiliza cookies para publicar anuncios en nuestro sitio. El uso de Google de la cookie DART le permite publicar anuncios a nuestros usuarios en función de su visita a nuestros sitios y otros sitios en Internet. Los usuarios pueden optar por no usar la cookie de DART visitando la política de privacidad de la red de contenido y anuncios de Google. <br> <br>

<strong> Enlaces a otros sitios </strong>
<br>
Este sitio incluye enlaces a sitios web externos. Animamos a los usuarios a revisar la política de privacidad de cada sitio vinculado, ya que no podemos ser responsables de las prácticas de privacidad de otros sitios. <br> <br>

<strong> Información de contacto </strong>
<br>
Si tiene preguntas o sugerencias sobre nuestra política de privacidad, contáctenos.
Teléfono: 305-6673774',
'warranties' => 'Al realizar pedidos de compra con NEXTOUCH LLC. usted acepta las tarifas, el plazo de pago, los gastos de envío y las condiciones. Nos reservamos el derecho en cualquier momento después de recibir su (s) pedido (s) de compra para rechazar el (los) pedido (s) por cualquier motivo. Todos los clientes nuevos deben pagar por adelantado el 100% del primer pedido inicial; El pago puede hacerse con tarjeta de crédito o transferencia bancaria. <br>
<br>

<strong> Envío y manipulación </strong>
<br>
De acuerdo con nuestro programa de trabajo, generalmente un pedido puede enviarse el mismo día si ha enviado el pedido antes de las 11 am, pero no está garantizado. Si el pedido no se puede enviar en el mismo día, generalmente el pedido se enviará el siguiente día hábil. Tenemos USPS y otras compañías de envío para proporcionar el mejor servicio para satisfacer sus necesidades comerciales y para garantizar la entrega a tiempo. Es su responsabilidad aceptar la entrega y cualquier cargo que pueda aplicarse, incluidos los cargos adicionales por los servicios que solicitó, es decir, enrutamiento especial, entrega interna, etc.

Se pueden aplicar cargos de envío, cargos de reenvío y una tarifa de reposición del 30% a todos los paquetes rechazados o devueltos
<br> <br> <br>
<strong> Recibiendo </strong>
<br>
Inspeccione la (s) caja (s) en busca de signos de daños antes de abrir su producto. Si cree que puede haber daños en el producto, tome una foto de la caja sin abrir. Esto ayudará a probar que el daño fue culpa del transportista. Conserve el empaque original hasta que haya verificado que su producto funciona como se espera de acuerdo con la garantía y la política.

Nota: Una vez que su pedido ha sido enviado a su ubicación, tiene hasta 24 horas para informar sobre cualquier falta o producto incorrecto.
<br> <br> <br>
<strong> Información sobre la garantía limitada </strong>
<br>
Nuestros productos están respaldados por diferentes garantías limitadas. Esta información de garantía limitada es solo para productos defectuosos. Lea cada garantía para determinar qué términos y condiciones se aplican.

Las garantías limitadas de 10 días se aplican a todos nuestros productos, excepto los accesorios Bluetooth.
Las garantías limitadas de 10 días se aplican a los accesorios Bluetooth.

No somos responsables de ningún uso indebido, abuso o daños causados ​​por un error humano en la instalación. Para obtener más información, llame a nuestro Departamento de Ventas al 305-6673774 <br> <br>

<strong> Cancelación </strong>
<br>
Usted es responsable del 30% de la tarifa de reposición de existencias para el pedido que se cancela después de la S.O. Se emite y procesa. La definición de Procesado es cuando el producto solicitado se ha reservado para el cumplimiento del pedido. Esta será una respuesta automática si no recibimos ninguna notificación suya durante 7 días hábiles. <br> <br>

<strong> Devolución e intercambio </strong>
<br>
Ofrecemos devolución de 5 días o cambio en buenos productos. Todos los productos de devolución y cambio deben estar en su estado original. Un crédito o cambio puede ser emitido por solicitud en la inspección. El costo de envío no es reembolsable. El intercambio puede ser aceptado por cualquier producto defectuoso. Consulte la información sobre la garantía limitada y el formulario RMA para obtener más información. No aceptamos ninguna devolución en todos los pedidos personalizados a menos que sea un accidente provocado internamente. Todos los productos de liquidación / liquidación son venta final; Sin cambio, sin devolución y sin devolución.',
'Purchase Policy' => 'Política de compra',
'Search entire store here...' => 'Buscar en toda la tienda aquí ...',
'Categories' => 'Categorías',
'Cart' => 'Carrito',
'address' => 'Contáctenos',
'Information' => 'información',
'Make an Appointment' => 'Hacer una cita',
'My account' => 'Cuenta',
'Sign In' => 'Iniciar sesión',
'View Cart' => 'Ver carrito',
'Track My Order' => 'Rastrear mi orden',
'Warranties' => 'Garantías',
'Franchise' => 'franquicia',
'Support' => 'Soporte',
'Follow us on :' => 'Síguenos en:',
'Remember Me' => 'Recuerdame',
'Already have an account with us' => 'Ya tienes una cuenta con nosotros.',
'Login Here' => 'Inicie sesión aquí',
'Forgot your password?' => '¿Olvidaste tu contraseña?',
'New to Nextouch?' => '¿Nuevo en Nextouch?',
'Create New Account' => 'Crear nueva cuenta',
'Customer Login' => 'Inicio de sesión del cliente',
'Sign off' => 'Cerrar sesion',
'All Categories' => 'Todas las categorias',
'products of interest' => 'productos de interes',
'Product' => 'Producto',
'Product Name' => 'Nombre producto',
'Price' => 'Precio',
'Quantity' => 'Cantidad',
'Action' => 'Acción',
'Success!' => 'Exito!',
'Product have added to your list.' => 'El producto se ha añadido a su lista.',
'Continue Shopping' => 'Seguir comprando',
'Cart Total' => 'Carrito total',
'Item(s) Subtotal' => 'Artículo(s) Subtotal',
'Shipping' => 'Envío',
'Amount Payable' => 'Cantidad pagable',
'Proceed to checkout' => 'Pasar por la caja',
'$' => '€',
'Checkout' => 'Envío',
'Search' => 'Buscar',
'Cart Subtotal' => 'Carrito Subtotal',
'Shopping' => 'Comprar',
'Add to Cart' => 'Agregar al carrito',
'Description' => 'Descripción',
'Availability' => 'Disponibilidad',
'account' => 'Cuenta',

'aboutDES' => 'NEXTOUCHONLINE.COM es una compañía que repara teléfonos celulares y tabletas a un costo muy bajo. Estamos orgullosos de tratar cada reparación como si el teléfono fuera nuestro.
Ofrecemos a nuestros clientes una solución completa para que sus dispositivos celulares y tabletas defectuosos vuelvan a funcionar correctamente. Reparar su teléfono puede ser muy costoso a través de su operador de telefonía celular, a menudo le deja solo con la opción de comprar un teléfono nuevo. Si no es elegible para la actualización, esta puede ser una opción muy costosa.

<br> <br>
Estamos comprometidos a ayudarlo a extender la vida útil de sus productos al ofrecer la mejor solución para restaurar el dispositivo a su condición original. Al final, lo que es importante para usted es importante para nosotros, y si necesita reemplazar la pantalla LCD rota o si instalamos un nuevo teclado, NEXTOUCHONLINE.COM lo ayudará a encontrar la solución adecuada de manera rápida y conveniente.
<br>
<br>

<strong> Sede corporativa: </strong> 5701, SUNSET DR UNIT 219, SOAMH MIAMI, FL 33143 | PH: 305-6673774

<br>
<br>
<strong> Servicio al cliente </strong>
<br>
Nuestra misión es asegurarnos de que encuentre la reparación exacta que necesita. Si decide visitarnos, encontrará representantes de servicio al cliente de primera categoría que están esperando para ayudarlo. Nuestros expertos en telefonía y tabletas son expertos, experimentados y dedicados a garantizar que su experiencia de reparación sea placentera.
<br> <br>
<strong> Entrega rápida a nivel nacional </strong>
<br>
Procesamos los pedidos de manera rápida y precisa, y enviamos la mayoría de nuestros teléfonos reparados en un día hábil. Si tiene prisa para que su teléfono vuelva a funcionar, ha venido al lugar correcto ',
'Your transaction is Approved' => 'Su transacción está aprobada',
'We will contact you' => 'Nos pondremos en contacto contigo',
'We are verifying the transaction' => 'Estamos verificando la transacción',
'Your transaction is in process' => 'Su transacción está en proceso',
'message_init_account' => 'Bienvenido a tu cuenta, aqui prodrás ver los ordenes y los detalles de tus productos solicitados',

'web_title' => 'NexTouch - Accesorios - Partes para móviles y tabletas',
'metaKeywords' => 'NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles',
'metaDescription' => 'NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles, Reparar tu teléfono y tableta es lo que mejor hacemos. compañía que repara teléfonos celulares y tabletas a un costo muy bajo. Estamos orgullosos de tratar cada reparación como si el teléfono fuera nuestro. Ofrecemos a nuestros clientes una solución completa para que sus dispositivos celulares y tabletas defectuosos vuelvan a funcionar correctamente. ',
'metaImage' => 'https://nextouchonline.com/themes/nexttouch/img/logo/logoespanol.png',
'metaAutor' => 'NexTouch',
'metaUrl' => 'https://nextouchonline.com/',
'metaSite' => '@nextouchonline',
'metaCreator' => '@nextouchonline',

'metaTitle_shop' => 'Tienda - NexTouch - Accesorios - Partes para móviles y tabletas',
'metaKeywords_shop' => 'Tienda - NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles',
'metaDescription_shop' => 'Tienda - ',

'metaTitle_about' => 'Sobre Nosotros - NexTouch - Accesorios - Partes para móviles y tabletas',
'metaKeywords_about' => 'Sobre Nosotros - NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles',
'metaDescription_about' => 'Sobre Nosotros - ',

'metaTitle_contact' => 'Contacto - NexTouch - Accesorios - Partes para móviles y tabletas',
'metaKeywords_contact' => 'Contacto - NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles',
'metaDescription_contact' => 'Contacto - ',

'metaTitle_faq' => 'Preguntas Frecuentes - NexTouch - Accesorios - Partes para móviles y tabletas',
'metaKeywords_faq' => 'Preguntas Frecuentes - NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles',
'metaDescription_faq' => 'Preguntas Frecuentes - ',

'metaTitle_policy' => 'Póliticas de privacidad - NexTouch - Accesorios - Partes para móviles y tabletas',
'metaKeywords_policy' => 'Póliticas de privacidad - NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles',
'metaDescription_policy' => 'Póliticas de privacidad - ',

'metaTitle_warranties' => 'Garantías - NexTouch - Accesorios - Partes para móviles y tabletas',
'metaKeywords_warranties' => 'Garantías - NexTouch - Accesorios - Partes, para móviles y tabletas, productos, telefonos, móviles',
'metaDescription_warranties' => 'Garantías - ',
'ES' => 'España',
'VE' => 'Venezuela',
'US' => 'Estados Unidos',
'seleccione' => 'Seleccione país',
'New Arrivals' => 'Productos',
'carprod' => 'El producto se ha añadido a su lista de deseos.',
'RegisExitoso' => 'Registro exitoso en NEXTOUCH',
'comunidad' => 'perteneces a la comunidad de NEXTOUCH',
'solicitudExi' => 'Solicitud de cuenta creada',
'visitenos' => 'Visitenos en https://nextouchonline.com/',
'felicidades' => 'Felicidades',
'Scroll-top' => 'Subir',
'formulario' => 'Envienos un mensaje a travez del formulario de contacto',
'clienteNuevo' => '¿Eres un cliente nuevo? Empieza aqui',
'mensajeContact' =>'Nuevo mensaje de contacto desde NEXTOUCH',
'mensajeExitoso' => 'Mensaje enviado correctamente, pronto te responderemos.',
'ActAprobado' => 'Aprobado',
'ActRechazado' => 'Rechazado',
'textActRechazado' => '¿Esta seguro de actualizar el estatus a rechazado?',
'textActAprobado' => '¿Esta seguro de actualizar el estatus a aprobado?',
'estatusPedidoAprobado' =>  'El pedido ha sido aprobado por  NEXTOUCHONLINE.COM',
'estatusPedidoRechazado' =>  'El pedido ha sido rechazado por NEXTOUCHONLINE.COM',
'tipoOrdenAprobada' => 'Aprobada la orden con el número:',
'tipoOrdenRechazada' => 'Rechazada la orden con el número:',
'informacionAprobada' => 'Próximamente le enviaremos su producto(s)',
'informacionRechazada' => 'Para mas información comuniquese con nosotros desde nuestra web https://nextouchonline.com/',
'ActEnviado' => 'Enviado',
'textActEnviado' => '¿Esta seguro de actualizar el estatus a enviado?',
'estatusPedidoEnviado' =>  'El pedido ha sido enviado por  NEXTOUCHONLINE.COM',
'tipoOrdenEnviado' => 'La orden con el número:',
'informacionEnviado' => 'Producto(s) enviado desde  NEXTOUCHONLINE.COM',
'correInvalido' => 'Correo inválido. Por favor ingrese nuevamente su correo.',
'recuperarContrasena' => 'Por favor, ingrese su correo para recuperar contraseña',
'ingreseCorreo' => 'Ingrese su correo',
'enviar' => 'Enviar',
'cambioContrasena' => 'CAMBIO DE CONTRASEÑA APROBADA DESDE NEXTTOUCH',
'codigoRecuperacion' => '>Codigo de recuperación:',
'ingreseCodigoText' => 'Por favor, ingrese el código que le fue enviado a su correo',
'ingreseCodigo' => 'Ingrese su código',
'claveInvalido' => 'código inválido. Por favor ingrese nuevamente su código.',
'ingreseContrasenaNueva' => 'Ingrese su nueva contraseña',
'ingreseContrasenaNuevaText' => 'Por favor, ingrese su nueva contraseña mayor a 8 dígitos:',
'ingreseNuevaClave' => 'Ingrese su nueva clave',
'repitaNuevaClave' => 'Repita su nueva clave',
'claveMayor' => 'Clave debe ser igual o mayor a 8 digitos',
'claveDiferente' => 'Las claves son diferentes',
'cambioExitoso' => 'Cambio de clave exitoso',
'detalle' => 'Ver detalle',
'orders' => 'Mis ordenes',
'accountDas' => 'Panel de cuenta',
'hello' => 'Hola',
'fechaHora' => 'Fecha y hora',
'total' => 'Total',
'opciones' => 'Opciones',
'detalle' => 'Ver detalle',
'details' => 'Detalles',
'datosPay' => 'Datos de PayPal',
'datosGenerales' => 'Datos generales',
'estadoCompra' => 'Estado de la compra',
'aprobada' => 'Aprobada',
'noaprobada' => 'No Aprobada',
'ordOver' => 'Order Overview',
'product' => 'Product',
'productDetail' => 'Product Detail',
'subTotal' => 'Sub Total',
'cartTotal' => 'Cart Total',
'itemSub' => 'Item(s) Subtotal',
'ship' => 'Shipping',
'amouPay' => 'Amount Payable',


			
	);

?>