<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'usuario_id') ?>
    <?= $form->field($model, 'usuario_nombre') ?>
    <?= $form->field($model, 'usuario_apellido') ?>
    <?= $form->field($model, 'usuario_login') ?>
    <?= $form->field($model, 'usuario_password') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
