<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
Use yii\helpers\ArrayHelper;

?>
<style>

  span.required {
  color: #ff0000;
}
</style>

  <?php if (Yii::$app->session->hasFlash('success')): ?>

      <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

         <?= Yii::$app->session->getFlash('success') ?>
      </div>

  <?php endif; ?>

<?php

        $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'method' => 'post',
            'id' => 'form-usuarios-create'
            ]);

?>



<div class="row">
    <div class="col-md-12 col-sm-12">

        <?= $form->errorSummary($model); ?>

    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-6">

        <?= $form->field($model, 'usuario_nombre')->textInput(['maxlength' => true,'class' => 'form-control','placeholder' => 'Ingrese un Nombre'])->label('Nombre') ?>

    </div>
    <div class="col-md-3 col-sm-6">

        <?= $form->field($model, 'usuario_apellido')->textInput(['maxlength' => true,'placeholder' => 'Ingrese un Apellido'])->label('Apellido') ?>

    </div>
    <div class="col-md-2">

    <?php
        
        if(Yii::$app->user->identity->usuario_perfil=='administrador') {

            echo $form->field($model, 'usuario_perfil')->dropDownList(
                Yii::$app->params['usuariosPerfiles'],
                ['prompt'=>'Seleccione...'])->label('Perfil');
            }
    ?>

    </div>
</div>
<div class="row">
    <div class="col-md-4">
        
        <?= $form->field($model, 'usuario_email')->textInput(['placeholder' => 'E-mail'])->label('E-mail') ?>
        
    </div>
    <div class="col-md-4">     

        <?= $form->field($model, 'usuario_activo')->checkBox(array('value'=>'1')) ?>
        
    </div>
</div>
<div class="row">

    <?php
        
        if(!$model->isNewRecord){

            $model->usuario_password = "";
         }

    ?>

    <div class="col-md-4">

        <?= $form->field($model, 'usuario_password')->passwordInput(['placeholder' => 'Contraseña'])->label('Contraseña') ?>

    </div>

</div>

<div class="row">

        <div class="col-md-8 text-center">
            <h3>Escoje un avatar</h3>

        <?php 

            echo $form->field($model, 'usuario_imagen_1')
            ->hiddenInput()
            ->label(false);

            ?>

                <style>

                    .selectAvatar {

                        cursor: pointer;

                    }

                    .selectAvatar:HOVER{

                        opacity: 0.5;

                    }

                </style>

                <div>&nbsp;</div>

                <div id="avatarSeleccionado" class="text-center">

                    <?php

                        $homeUrl = Url::base(true).'/web/resources/AdminLTE/dist/img/avatars/';

                    ?>

                    <?php

                        if(!empty($model->usuario_imagen_1)){

                            $fotoAvatar = $homeUrl.$model->usuario_imagen_1;
                    ?>

                        <img src='<?php print $fotoAvatar; ?>' height='200' width='200' >

                    <?php

                        }

                    ?>

                </div>
                
                <div>&nbsp;</div>

                 <?php

                    $folder = Yii::$app->basePath.'/web/resources/AdminLTE/dist/img/avatars';

                    if(is_dir($folder)){

                        $folderContent = scandir($folder);

                        foreach($folderContent as $keyFolCon => $valFolCon){

                            // si esta la palabra avatar en el archivo

                            if(substr_count($valFolCon, 'avatar') > 0){

                    ?>

                        <img class="selectAvatar" alt="avatar" title="avatar" avatarName="<?php print $valFolCon; ?>" src="<?php print $homeUrl.$valFolCon; ?>" style="max-height: 85px; max-width: 85px;">

                    <?php
                    
                }
            }
         }

                    ?>

        </div>
            <div class="col-md-8 text-center">

            <div>&nbsp;</div>

             <?= Html::a(

                    $model->isNewRecord ? 'Crear' : 'Actualizar',

                    $model->isNewRecord ? ['users/create'] : ['users/update','id'=>$model->usuario_id],

                    [

                    'data' => [

                        'method' => 'post',

                        ],

                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'

                    ])?>

    <div>&nbsp;</div>

            </div>

    <div>&nbsp;</div>

</div>

<?php ActiveForm::end(); ?>