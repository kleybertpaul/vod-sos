<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'usuario_id')->textInput() ?>

    <?= $form->field($model, 'usuario_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_clave')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_correo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_fecha_de_nacimiento')->textInput() ?>

    <?= $form->field($model, 'usuario_grado_de_instruccion')->textInput() ?>

    <?= $form->field($model, 'pais_id')->textInput() ?>

    <?= $form->field($model, 'usuario_perfil')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
