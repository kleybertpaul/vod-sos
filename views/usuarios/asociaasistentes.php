<?php

use yii\helpers\Html;
use app\models\AsistentesDoctores;

$session = Yii::$app->session;
$sessionUsuarioNombre = $session['sessionSelectDoctor']['usuario_nombre']." ".$session['sessionSelectDoctor']['usuario_apellido'];

$this->title = Yii::t('app', 'Agregar asistente para '.$sessionUsuarioNombre);

$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-primary">
	<div class="box-body">
	<div class="title-bmo-10"> <?= Html::encode($this->title) ?></div>
	<hr class="hr-bmo-10 ">

	<?php if (Yii::$app->session->hasFlash('success')): ?>
	      <div class="alert alert-success alert-dismissable">
	        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	         <?= Yii::$app->session->getFlash('success') ?>
	      </div>
	  <?php endif; ?>
<?php
	if(!empty($modelAsistente)){
?>
	<h4>Hemos encontrado a un usuario bajo la identificación <strong><?php print $modelAsistente["fiscal_identificacion"]; ?></strong></h4>


	<strong>Nombre:</strong> <?php print $modelAsistente["usuario_nombre"]." ".$modelAsistente["usuario_apellido"]; ?><br>
	<strong>Cédula o Pasaporte:</strong> <?php print $modelAsistente["fiscal_identificacion"]; ?><br>
	<strong>E-Mail:</strong> <?php print $modelAsistente["usuario_email"]; ?><br>
	<strong>Teléfono:</strong> <?php print $modelAsistente["usuario_telefono_1"]; ?><br>
	<strong>Pefil de usuario:</strong> <?php print $modelAsistente["usuario_perfil"]; ?><br>
	<strong>Usuario desde:</strong> <?php print $modelAsistente["usuario_fecha_creacion"]; ?><br>
	<div>&nbsp;</div>
	<?php 
		if($modelAsistente["usuario_perfil"] == "asistente"){
	?>
			<form>

				<?php
					if(Yii::$app->user->identity->usuario_perfil == 'doctor'){
						$doctor = Yii::$app->user->identity->usuario_id;
						$asistente = $modelAsistente["usuario_id"];
						echo Html::hiddenInput('asocia_doctor', $doctor);
						echo Html::hiddenInput('asocia_asistente', $asistente);
					}

					$modelAsiDoc = AsistentesDoctores::find()->where(["users_doctor_id"=>$doctor,"users_asistente_id"=>$asistente])->one();
					//var_dump($modelAsiDoc);
				?>

			<?php if(empty($modelAsiDoc)){ ?>
			<?= Html::a(
				'Agregar a '.$modelAsistente["usuario_nombre"]." ".$modelAsistente["usuario_apellido"].' como asistente',
          		['usuarios/asociaasistentes'],
          		[
	            	'data' => [
	                	'method' => 'post',
	               	],
	            	'class' => 'btn btn-success',
	            	'onclick' => '  
				       	//var r = confirm("¿Está seguro de actualizar el precio actual de la consulta online?")
				       	//console.log(r)
				       	//if (r == false) return false
			        '  
            	])?>
            <?php } else { ?>
            	<div class="alert alert-info alert-dismissable" style="color: black !important;">
		         	<?php print $modelAsistente["usuario_nombre"]." ".$modelAsistente["usuario_apellido"]; ?> ya es su asistente.
		      	</div>
            <?php } ?>
            </form>


            <?php
            /*
			  echo Html::button('Actualizar', [ 
		  	  	'class' => 'btn btn-danger',
		      	'onclick' => '  
			       	var txt;
			       	var r = confirm("¿Está seguro de actualizar el precio actual de la consulta online?");
			       	if (r == true) {
			       		doctores.updatePrice()
			        }
		        '
		      ]);
		      */
			?>


	<?php
		} else {
	?>
			<div class="alert alert-warning alert-dismissable" style="color: black !important; font-weight: bold;">
	         	Este usuario tiene perfil de <?php print $modelAsistente["usuario_perfil"]; ?>.<br>
				Debe tener perfil de asistente para poder agregarlo.<br>
				Recuerde que nuestro equipo especializado de Soporte, le brindará toda la ayuda necesaria para resolver sus inconvenientes.
	      	</div>
			
	<?php
		}
	?>

	<?php // Html::a('Agregar Asistente', ['createasistentes'], ['class' => 'btn btn-success']) ?>
	<?php
		}
	?>
	</div>
</div>