<?php



use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use kartik\editable\Editable;
Use yii\helpers\ArrayHelper;
use app\models\Usuarios;



$this->title = 'Gestión de Usuarios';
$this->params['breadcrumbs'][] = $this->title;

?>





<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box box-primary">
				<div class="box-body">
				<div class="title-bmo-10"> <?= Html::encode($this->title) ?></div>	<div class="box-header">
<hr class="hr-bmo-10 ">

					<?php if(Yii::$app->session->hasFlash('exception')): ?>

				<div class="alert alert-danger alert-dismissable">

						<?php echo Yii::$app->session->getFlash('exception'); ?>

				</div>

				<?php endif; ?>



				          <?php if (Yii::$app->session->hasFlash('success')): ?>

				              <div class="alert alert-success alert-dismissable">

				                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

				               <?= Yii::$app->session->getFlash('success') ?>

				              </div>

				          <?php endif; ?>



			  <p>

		        <?= Html::a('Crear doctor', ['createdoctor'], ['class' => 'btn btn-success']) ?>

		    </p>





		    <?php



$dataProvider->query->andFilterWhere(['usuario_perfil'=>'doctor'])->orderBy(['usuario_id' => SORT_DESC]);

		    ?>
<div class="table-responsive">
		    <?= GridView::widget([

		        'dataProvider' => $dataProvider,

		        'filterModel' => $searchModel,

		 

		    

					



		        'columns' => [

		            ['class' => 'yii\grid\SerialColumn'],



		            'usuario_nombre',

		            'usuario_apellido',

		            'usuario_email:ntext',

      	            'usuario_perfil',

		          

[

                'attribute' => 'Estatus',

                'header' => 'Estatus',

                'format' => 'raw',

                

                'value' => function ($data) {

                  

          $data->usuario_activo = ($data->usuario_activo) ? '<h4><span class="label label-success">ACTIVO</span></h4>' : '<h4><span class="label label-danger">INACTIVO</span></h4>';



                

                $Prueba=array("0"=>"INACTIVO",

                        "1"=>"ACTIVO");

                  

                  $editable = Editable::widget([

                    'model' => $data,

                    'attribute' => 'usuario_activo',



                              

              

                    'type' => 'primary',

                    'format' => Editable::FORMAT_BUTTON,

                    'inputType' => Editable::INPUT_DROPDOWN_LIST,

                    'options' => ['class'=>'form-control', 'prompt'=>'Seleccione...', 'id' => $data->usuario_id ],

                    'data' => $Prueba,

           

                    'formOptions' => [ 'action' => [ 'editabledoctor', 'usuarios' => $data->usuario_id ]],

                  ]);

                  

                  



                   return $editable;

                    



                 

                },



              ],



		                [

                   'class' => 'yii\grid\ActionColumn',

                        'template' => '{update} ',

                                'header' => 'Acciones',

                              

                        'buttons' => ['update' => function ($url, $model) {

                                        return Html::a('<span class="fa fa-pencil"></span>', ['/usuarios/updatedoctor/'.$model->usuario_id], [

                            'title' => Yii::t('app', 'Actualizar'), ]);

                                }],

                                'visible' => (Yii::$app->user->identity->usuario_perfil=='administrador'),

                                'options' => ['width' => '120'],

                        ],

		        ],

		    ]); ?>

		     </div>

		      </div>





		</div>

	</div>

</div>

