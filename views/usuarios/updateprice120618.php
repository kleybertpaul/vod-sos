<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>

<?php
$form = ActiveForm::begin();
?>

 

<div class="box box-primary">
	<div class="box-body">
		<div class="title-bmo-10"> 
			Actualizar precios
		</div>
		<hr class="hr-bmo-10 ">

		<div class="row">
		  <div class="col-md-12 col-sm-12">
		    <?= $form->errorSummary($model); ?>
		  </div>
		</div>

		<div>&nbsp;</div>
		<div class="row">
		  <div class="col-md-12 col-sm-12">
		    <?php print Html::activeLabel($model, 'doctor_costo_consulta_online').": "; ?>
		    <span id="contentValuePrecio"><?php print (empty($model->doctor_costo_consulta_online)) ? "0" : $model->doctor_costo_consulta_online;  ?> USD </span>
		  </div>
		</div>	
		<div>&nbsp;</div>
		<div class="row">
		  <div class="col-md-12 col-sm-12">
		  	<strong>Nuevo precio:</strong>
		  	&nbsp;
		   	<select id="selectNuevoPrecio">
		   		<option value="1">1</option>
		   		<option value="2">2</option>
		   		<option value="3">3</option>
		   		<option value="4">4</option>
		   		<option value="5">5</option>
		   		<option value="6">6</option>
		   		<option value="7">7</option>
		   		<option value="8">8</option>
		   		<option value="9">9</option>

		   		<option value="10">10</option>
		   		<option value="11">11</option>
		   		<option value="12">12</option>
		   		<option value="13">13</option>
		   		<option value="14">14</option>
		   		<option value="15">15</option>
		   		<option value="16">16</option>
		   		<option value="17">17</option>
		   		<option value="18">18</option>
		   		<option value="19">19</option>

		   		<option value="20">20</option>
		   		<option value="21">21</option>
		   		<option value="22">22</option>
		   		<option value="23">23</option>
		   		<option value="24">24</option>
		   		<option value="25">25</option>
		   		<option value="26">26</option>
		   		<option value="27">27</option>
		   		<option value="28">28</option>
		   		<option value="29">29</option>

		   		<option value="30">30</option>
		   		<option value="31">31</option>
		   		<option value="32">32</option>
		   		<option value="33">33</option>
		   		<option value="34">34</option>
		   		<option value="35">35</option>
		   		<option value="36">36</option>
		   		<option value="37">37</option>
		   		<option value="38">38</option>
		   		<option value="39">39</option>

		   		<option value="40">40</option>
		   		<option value="41">41</option>
		   		<option value="42">42</option>
		   		<option value="43">43</option>
		   		<option value="44">44</option>
		   		<option value="45">45</option>
		   		<option value="46">46</option>
		   		<option value="47">47</option>
		   		<option value="48">48</option>
		   		<option value="49">49</option>

		   		<option value="50">50</option>
		   		<option value="51">51</option>
		   		<option value="52">52</option>
		   		<option value="53">53</option>
		   		<option value="54">54</option>
		   		<option value="55">55</option>
		   		<option value="56">56</option>
		   		<option value="57">57</option>
		   		<option value="58">58</option>
		   		<option value="59">59</option>


		   	</select>
		   	&nbsp;
		   	USD
		  </div>
		</div>


		<div>&nbsp;</div>
		<div>&nbsp;</div>
		<div id="contentBntActualizar">
		<?php

		  echo Html::button('Actualizar', [ 
	  	  	'class' => 'btn btn-danger',
	      	'onclick' => '  
		       	var txt;
		       	var r = confirm("¿Está seguro de actualizar el precio actual de la consulta online?");
		       	if (r == true) {
		       		doctores.updatePrice()
		        }
	        '
	      ]);
		?>
		</div>
		<div id="contentLoading" style="display: none; color: #D73925;">
			<i class="fa fa-spinner fa-spin" style="font-size: 20px;"></i> &nbsp; Actualizando...
		</div>

		<div>&nbsp;</div>
		<small>Nota: Tu Hospital Virtual, C.A. sumará 3.25 USD adicionales por comisión del servicio</small>

	</div>
</div>

<?php ActiveForm::end(); ?>



<script type="text/javascript">
	


	var doctores = {}

	doctores.updatePrice = function(){


		var selectNuevoPrecio = jQuery("#selectNuevoPrecio").val()

		jQuery.ajax({
	        url: CONFIGURATION.WEBSERVICE_CONTROLLER+'usuarios/updateprice',
	        method: 'POST',
	        dataType: 'json',
	        data : {
	        	"Doctores[doctor_costo_consulta_online]" : selectNuevoPrecio
	        },
	        success: function (data) {
			
				jQuery("#contentBntActualizar").show()
	          	jQuery("#contentLoading").hide()
	          	jQuery("#contentValuePrecio").html(data.value+ " USD actualizado <i class='fa fa-check'></i>").css({"color":"#3C8DBC"})

	          	//console.log(data)

			},
	        beforeSend: function (xhr) {
	          	
	          	jQuery("#contentBntActualizar").hide()
	          	jQuery("#contentLoading").show()

	        },
	        error: function (xhr, status) {

	          	jQuery("#contentBntActualizar").show()
	          	jQuery("#contentLoading").hide()

	        }

	    })

	}


</script>






