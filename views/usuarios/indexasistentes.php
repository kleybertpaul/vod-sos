<?php

use yii\helpers\Html;
use yii\grid\GridView;

$session = Yii::$app->session;
$sessionUsuarioNombre = $session['sessionSelectDoctor']['usuario_nombre']." ".$session['sessionSelectDoctor']['usuario_apellido'];

$this->title = Yii::t('app', 'Asistentes de '.$sessionUsuarioNombre);

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box box-primary">
				<div class="box-body">
				<div class="title-bmo-10"> <?= Html::encode($this->title) ?></div>	<div class="box-header">
					<hr class="hr-bmo-10 ">
					<?php if(Yii::$app->session->hasFlash('exception')): ?>
					<div class="alert alert-danger alert-dismissable">
					<?php echo Yii::$app->session->getFlash('exception'); ?>
				</div>
				<?php endif; ?>
		          <?php if (Yii::$app->session->hasFlash('success')): ?>
		              <div class="alert alert-success alert-dismissable">
		                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		               <?= Yii::$app->session->getFlash('success') ?>
		              </div>
		          <?php endif; ?>
			  <p><?= Html::a('Agregar Asistente', ['createasistentes'], ['class' => 'btn btn-success']) ?></p>
		    

		    <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        //'filterModel' => $searchModel,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		            [
		            	'header' => 'Nombre y apellido',
		            	//'attribute' => 'usuario_activo',
                  		'format' =>  'raw',
                    	'value'=> function($data){
                    		$html = $data->usersAsistente->usuario_nombre." ".$data->usersAsistente->usuario_apellido;
                         	return $html;
                       	}
		            ],
		            [
		            	'header' => 'E-Mail',
		            	//'attribute' => 'usuario_activo',
                  		'format' =>  'raw',
                    	'value'=> function($data){
                    		$html = $data->usersAsistente->usuario_email;
                         	return $html;
                       	}
		            ],
		            [
		            	'header' => '',
		            	//'attribute' => 'usuario_email',
                  		'format' =>  'raw',
                    	'value'=> function($data){
                    		$html = $data->usersAsistente->usuario_perfil." de ".$data->usersDoctor->usuario_nombre." ".$data->usersDoctor->usuario_apellido;
                         	return $html;
                       	}
		            ],
		            



		            [
		            	'class' => 'yii\grid\ActionColumn',
                        'template' => ''
                    ],

		        ],

		    ]); ?>



		      </div>





		</div>

	</div>

</div>

