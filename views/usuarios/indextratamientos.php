<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use kartik\editable\Editable;
Use yii\helpers\ArrayHelper;
use app\models\Usuarios;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tratamientos';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
	<div class="col-md-12">
		<div class="box">


			<div class="box box-primary">
				<div class="box-body">
				<div class="title-bmo-10"> <?= Html::encode($this->title) ?></div>	<div class="box-header">
<hr class="hr-bmo-10 ">
					<?php if(Yii::$app->session->hasFlash('exception')): ?>
				<div class="alert alert-danger alert-dismissable">
						<?php echo Yii::$app->session->getFlash('exception'); ?>
				</div>
				<?php endif; ?>

				          <?php if (Yii::$app->session->hasFlash('success')): ?>
				              <div class="alert alert-success alert-dismissable">
				                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				               <?= Yii::$app->session->getFlash('success') ?>
				              </div>
				          <?php endif; ?>

			  <p>
		    </p>
		  

		    <?php

		   
		   if(Yii::$app->user->identity->usuario_perfil=='doctor')
		    	{
		    	

$usuarioid = Yii::$app->user->identity->usuario_id;
$dataProvider->query->andFilterWhere(['doctor_id'=>$usuarioid])->groupBy(['paciente_id']);

 print GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		 
		    
					

		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		           
		            //'medicamento_id.medicamento_nombre',
		            'cita_medica_id.tratamiento_cantidad',
		            'cita_medica_id.tratamiento_dias',
		            'cita_medica_id.tratamiento_fecha_comienzo',
		            'cita_medica_id.tratamiento_fecha_hora',
		            	            
		             

                        


[
                   'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                                'header' => 'Acciones',
                        
                                'visible' => (Yii::$app->user->identity->usuario_perfil=='administrador'),
                                'options' => ['width' => '120'],
                        ],
		         
		        ],
		    ]); 


		    	} else if (Yii::$app->user->identity->usuario_perfil=='administrador') {


		    		$dataProvider->query->andFilterWhere(['usuario_perfil'=>'paciente'])->orderBy(['usuario_id' => SORT_DESC]);

		    		 print GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		 
		    
					

		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		            'usuario_nombre',
		            'usuario_apellido',
		            
		             'usuario_email:ntext',
		            





     

                           
		            
		             'usuario_perfil',
		             
[
                   'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                                'header' => 'Acciones',
                        'buttons' => ['view' => function ($url, $model) {
                                        return Html::a('<span class="fa fa-eye"></span>', ['/usuarios/'.$model->usuario_id], [
                            'title' => Yii::t('app', 'Ver'), ]);
                                }],
                                'visible' => (Yii::$app->user->identity->usuario_perfil=='doctor'),
                                'options' => ['width' => '120'],
                        ],



[
                   'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                                'header' => 'Acciones',
                        
                                'visible' => (Yii::$app->user->identity->usuario_perfil=='administrador'),
                                'options' => ['width' => '120'],
                        ],
		         
		        ],
		    ]); 

		    	} else {

		    		$dataProvider->query->andFilterWhere(['usuario_perfil'=>'prueba'])->orderBy(['usuario_id' => SORT_DESC]);

		    	}

		    ?>
		   

		      </div>


		</div>
	</div>
</div>
