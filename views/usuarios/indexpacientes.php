<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use kartik\editable\Editable;
Use yii\helpers\ArrayHelper;
use app\models\Usuarios;

$session = Yii::$app->session;
$sessionUsuarioNombre = $session['sessionSelectDoctor']['usuario_nombre']." ".$session['sessionSelectDoctor']['usuario_apellido'];

$this->title = 'Listado de Pacientes de '.$sessionUsuarioNombre;

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box box-primary">
				<div class="box-body">
				<div class="title-bmo-10"> <?= Html::encode($this->title) ?></div>	<div class="box-header">

						<hr class="hr-bmo-10 ">	

					<?php if(Yii::$app->session->hasFlash('exception')): ?>
				<div class="alert alert-danger alert-dismissable">
						<?php echo Yii::$app->session->getFlash('exception'); ?>
				</div>

				<?php endif; ?>

				          <?php if (Yii::$app->session->hasFlash('success')): ?>
				              <div class="alert alert-success alert-dismissable">
				                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				               <?= Yii::$app->session->getFlash('success') ?>
				              </div>
				          <?php endif; ?>
			  <p>

		    </p>

	<div class="table-responsive">		 
	<?php
		if(Yii::$app->user->identity->usuario_perfil=='doctor' || Yii::$app->user->identity->usuario_perfil=='asistente'){

			$doctor = Yii::$app->user->identity->usuario_id;
			if (isset($session['sessionSelectDoctor'])) {
			  $doctor = $session['sessionSelectDoctor']['usuario_id'];
			}

			$dataProvider->query
			->select('paciente_id,familiar_id')
			->Where('doctor_id='.$doctor.' AND cita_diagnostico is NOT null AND cita_diagnostico != ""')
			->groupBy(['paciente_id','familiar_id']);

			

		 	print GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
	            	[            
		            	'header' => 'Nombre del paciente',
		            	'format'=>'raw',
		            	'value'=> function($model){
		            		if($model->familiar_id ==null) {
			 					$txt = $model->paciente->usuario_nombre." ".$model->paciente->usuario_apellido.
			 					"<br><small>Paciente princial (".$model->paciente->usuario_email.")</small>";
		            		} else {
			 					$txt = $model->familiar->familiar_nombre." ".$model->familiar->familiar_apellido.
			 					"<br><small>Paciente famililiar de ".$model->paciente->usuario_nombre." ".$model->paciente->usuario_apellido."</small>";
		            		}
		            		return   $txt ;
		          		}
		          	],	
					[
                   	'class' => 'yii\grid\ActionColumn',
                        'template' => '{citas}',
                        'header' => 'Acciones',            
   						'buttons' => [
   							'citas' => function ($url, $model) {
                                return Html::a('<span class="fa fa-calendar"></span>', ['/citasmedicas/indexcitaspacientes?codpac='.$model->paciente_id.'&codfam='.$model->familiar_id], [
                        		'title' => Yii::t('app', 'Ver citas atendidas'), ]);
                            }],

                        'visible' => (Yii::$app->user->identity->usuario_perfil=='doctor' || Yii::$app->user->identity->usuario_perfil=='asistente'),
                        'options' => ['width' => '120'],
                    ],
		        ],
		    ]); 



		} else if (Yii::$app->user->identity->usuario_perfil=='administrador') {

		    		$dataProvider->query->andFilterWhere(['usuario_perfil'=>'paciente'])->orderBy(['usuario_id' => SORT_DESC]);
		    		 print GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		            'usuario_nombre',
		            'usuario_apellido',
		            'usuario_email:ntext',
		            'usuario_perfil',
					[
                'attribute' => 'Estatus',
                'header' => 'Estatus',
                'format' => 'raw',
                'value' => function ($data) {                  

          	$data->usuario_activo = ($data->usuario_activo) ? '<h4><span class="label label-success">ACTIVO</span></h4>' : '<h4><span class="label label-danger">INACTIVO</span></h4>';
                $Prueba=array("0"=>"INACTIVO","1"=>"ACTIVO");
                  $editable = Editable::widget([
                    'model' => $data,
                    'attribute' => 'usuario_activo',   
                    'type' => 'primary',
                    'format' => Editable::FORMAT_BUTTON,
                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    'options' => ['class'=>'form-control', 'prompt'=>'Seleccione...', 'id' => $data->usuario_id ],
                    'data' => $Prueba,
                    'formOptions' => [ 'action' => [ 'editable', 'usuarios' => $data->usuario_id ]],
                  ]);
                   return $editable;
                },
              ],
				[
                   'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                                'header' => 'Acciones',
                        'buttons' => ['view' => function ($url, $model) {
                                        return Html::a('<span class="fa fa-eye"></span>', ['/usuarios/'.$model->usuario_id], [
                            'title' => Yii::t('app', 'Ver'), ]);
                                }],
                                'visible' => (Yii::$app->user->identity->usuario_perfil=='doctor' || Yii::$app->user->identity->usuario_perfil=='asistente'),
                                'options' => ['width' => '120'],
                        ],
[
                   'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} ',
                                'header' => 'Acciones',     
                                'visible' => (Yii::$app->user->identity->usuario_perfil=='administrador'),
                                'options' => ['width' => '120'],
                        ],
		        ],
		    ]); 


		    	} else {
		    		$dataProvider->query->andFilterWhere(['usuario_perfil'=>'prueba'])->orderBy(['usuario_id' => SORT_DESC]);


		    	}


		    ?>

		    </div>



		      </div>
		</div>
	</div>
</div>

