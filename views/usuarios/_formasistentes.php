<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
use app\models\Paises;
Use yii\helpers\ArrayHelper;

?>



<style>

  span.required {

  color: #ff0000;

}

</style>

  <?php if (Yii::$app->session->hasFlash('success')): ?>
      <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('success') ?>
      </div>
  <?php endif; ?>


<?php if(empty($model->usuario_identificacion)){ ?>
<div class="row">
	<div class="col-md-3">
		<?php
		$form1 = ActiveForm::begin([
			'method' => 'post',
			'id' => 'form-verifica-asistente'
		]);
		?>
		<?php
		echo $form1->field($model, 'usuario_identificacion')->textInput(['placeholder' => 'Cédula o Pasaporte ']);
		?>
		<?= 
		Html::a(
			'Buscar Asistente',
			['usuarios/buscarasistentes'],
 		[
    	'data' => [
            'method' => 'post',
        	],
				'class' => 'btn btn-warning'
		])?>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div>&nbsp;</div>

<?php } else { ?>



	<div class="alert alert-info alert-dismissable" style="">
     	Complete los datos del siguiente formulario para agregar a su asistente
  	</div>



		<?php
			$form = ActiveForm::begin([
				    'options' => ['enctype' => 'multipart/form-data'],
					'method' => 'post',
					'id' => 'form-usuarios-create'
				]);
		?>

<div class="row">
	<div class="col-md-12 col-sm-12">
		<?= $form->errorSummary($model); ?>
	</div>
</div>


<div class="col-md-6">
<div class="row">
	<div class="col-md-6">
	<?php 
	  	$model->usuario_genero = '2';
	  	echo $form->field($model, 'usuario_genero')->radioList( Yii::$app->params['genero'],['inline'=>true]);
    ?>
	</div>
	<div class="col-md-6">
   		<?php 
            $ts   = strtotime($model->usuario_fecha_nacimiento);
            $a = date("d-m-Y", $ts);

            $fechaMenos18Anos = date('d-m-Y', strtotime('-18 years'));

   	        echo '<label class="control-label">Fecha de nacimiento (18+ años) </label>';
   	        /*
       		echo DatePicker::widget([
                'model' => $model, 
                'attribute' => 'usuario_fecha_nacimiento',
                'options' => [
                'value' => $a],
                'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-mm-yyyy'
                ]
            ]);
            */	

            // http://demos.krajee.com/widget-details/datepicker


       		echo DatePicker::widget([
                'model' => $model, 
                'attribute' => 'usuario_fecha_nacimiento',
                'options' => [
                	'value' => $fechaMenos18Anos,
            	],
                "type" => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                	//"endDate" => date("2000-06-01 01:01:01"),
                	//'startDate' => date('2000-07-01 11:00:00'),
					//'endDate' => date('2000-07-20 11:00:00'),
                	//'minDate' => date('2000-07-01 11:00:00'),
                	//'endDate' => date('d-m-Y'), // funciona
                	'endDate' => $fechaMenos18Anos, // funciona
	                'autoclose'=>true,
	                'format' => 'dd-mm-yyyy'
                ]
            ]);	



/*
            field($dir, "[{$index}]owner_dob")->widget(DatePicker::classname(), [
			"options" => ["placeholder" => "Date of Birth ..."],
			"id" => "dating",
			"type" => DatePicker::TYPE_COMPONENT_APPEND,
			"pluginOptions" => [
				"endDate" => date("2000-01-01 01:01:01"),
				"format" => "dd-M-yyyy",
				"autoclose"=>true,
			]])->label(false);?>
*/



       	?>

   	</div>
</div>
<div class="row">
			<div class="col-md-6 col-sm-12">
				<?= $form->field($model, 'usuario_nombre')->textInput(['maxlength' => true,'class' => 'form-control','placeholder' => 'Ingrese un Nombre']) ?>
			</div>
			<div class="col-md-6 col-sm-12">
				<?= $form->field($model, 'usuario_apellido')->textInput(['maxlength' => true,'placeholder' => 'Ingrese un Apellido'])  ?>
			</div>
		</div>
		<div class="row">
           <div class="col-md-6 col-sm-12">
           		<?php
      			echo $form->field($model, 'usuario_identificacion')->textInput(['placeholder' => 'Cédula o Pasaporte ']);

				?>
           </div>
           <div class="col-md-6 col-sm-12">
           		<?php
      			echo $form->field($model, 'usuario_identificacion_confirmacion')->textInput(['placeholder' => 'Cédula o Pasaporte ']);
				?>
           </div>
        </div>
		<div class="row">
			<div class="col-md-6 col-sm-12">
			<?= $form->field($model, 'usuario_email')->textInput(['placeholder' => 'E-mail'])?>
		</div>
		<div class="col-md-6 col-sm-12">
			<?= $form->field($model, 'usuario_email_confirmacion')->textInput(['placeholder' => 'E-mail']) ?>
		</div>
	</div>
	<div class="row">
		<?php
		if(!$model->isNewRecord){
			$model->usuario_password = "";
			$model->usuario_password_confirmacion ="";
		}
		?>
		<div class="col-md-6 col-sm-12">
			<?= $form->field($model, 'usuario_password')->passwordInput(['placeholder' => 'Contraseña']) ?>
		</div>
		<div class="col-md-6 col-sm-12">
			<?= $form->field($model, 'usuario_password_confirmacion')->passwordInput(['placeholder' => 'Contraseña']) ?>
		</div>
	</div>
           <div class="row">
           <div class="col-md-6 col-sm-12">
           	   <?php echo $form->field($model, 'usuario_telefono_1')->textInput([  'placeholder' => ' Teléfono principal']);

    ?>
           </div>
           <div class="col-md-6 col-sm-12">

           	 <?php echo $form->field($model, 'usuario_telefono_emergencia')->textInput([ 'placeholder' => 'Teléfono de emergencia']);
    ?>
           </div>
	</div>
</div>

<div class="col-md-6">
	<div class="row">
		<div class="col-md-12 text-center">
			<h3>Escoje un avatar</h3>
    	<?php 
			echo $form->field($model, 'usuario_imagen_1')
			->hiddenInput()
			->label(false);
			?>
				<style>
			    	.selectAvatar {
			    		cursor: pointer;
			    	}
			    	.selectAvatar:HOVER{
			    		opacity: 0.5;
			    	}
			    </style>
			    <div>&nbsp;</div>
				<div id="avatarSeleccionado" class="text-center">
					<?php
						$homeUrl = Url::base(true).'/web/resources/AdminLTE/dist/img/avatars/';

						if(!empty($model->usuario_imagen_1)){
							$fotoAvatar = $homeUrl.$model->usuario_imagen_1;
					?>
						<img src='<?php print $fotoAvatar; ?>' height='200' width='200' >
					<?php
			 			}
					?>
				</div>
				<div>&nbsp;</div>
				 <?php
				    $folder = Yii::$app->basePath.'/web/resources/AdminLTE/dist/img/avatars';
				    if(is_dir($folder)){
				    	$folderContent = scandir($folder);
				    	foreach($folderContent as $keyFolCon => $valFolCon){
				    		// si esta la palabra avatar en el archivo
				    		if(substr_count($valFolCon, 'avatar') > 0){
				    ?>
				    	<img class="selectAvatar" alt="avatar" title="avatar" avatarName="<?php print $valFolCon; ?>" src="<?php print $homeUrl.$valFolCon; ?>" style="max-height: 85px; max-width: 85px;">
				    <?php
				    		}
				    	}
				    }
				    ?>
		</div>
	</div>		
</div>

<div class="col-md-12">
	<div>&nbsp;</div>

    <?php
        if(!$model->isNewRecord){
            echo Html::hiddenInput('validate_usuario_id', $model->usuario_id);
        }
        
    ?>
	 <?= Html::a(
	 		$model->isNewRecord ? 'Agregar Asistente' : 'Actualizar Asistente',
	 		$model->isNewRecord ? ['usuarios/createasistentes'] : ['usuarios/updateasistentes','id'=>$model->usuario_id],
	 		[
        	'data' => [
	            'method' => 'post',
	        	],
 				'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
    		])?>
	<div>&nbsp;</div>
</div>	
<?php ActiveForm::end(); ?>





<?php } ?>