<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
use app\models\Especialidades;
use app\models\Doctores;
Use yii\helpers\ArrayHelper;
use app\models\Paises;
use app\models\SolicitudesRegistro;
use kartik\select2\Select2;


if(isset($_REQUEST["code"])){


	$code = $_REQUEST["code"];
	$modelSolicitudesRegistro = SolicitudesRegistro::find()->where("solicitud_registro_id = $code ")->one();
	$model->usuario_nombre = $modelSolicitudesRegistro->solicitud_registro_nombre;
  $model->usuario_apellido = $modelSolicitudesRegistro->solicitud_registro_apellido;
	$model->usuario_email = $modelSolicitudesRegistro->solicitud_registro_email;
	$model->usuario_email_confirmacion = $modelSolicitudesRegistro->solicitud_registro_email;
	$model->pais_id = $modelSolicitudesRegistro->solicitud_registro_pais_id;
	$model->usuario_telefono_1 = $modelSolicitudesRegistro->solicitud_registro_telefono;
  $model->usuario_identificacion =   $modelSolicitudesRegistro->solicitud_registro_identificacion;
  $modeldoctores->doctor_numero_colegialo =   $modelSolicitudesRegistro->solicitud_registro_numero_colegiado;
  $modeldoctores->doctor_imagen = $modelSolicitudesRegistro->solicitud_registro_imagen;
  $modeldoctores->doctor_especialidades = $modelSolicitudesRegistro->solicitud_registro_especialidad;


}



?>



<style>

  span.required {
  color: #ff0000;

}

</style>

  <?php if(!$model->isNewRecord){ ?>
  <div class="row">
      <div class="col-md-12 col-sm-12">
          <a href="../../doctores/centros/<?php print $modeldoctores->doctor_id;?>?idpais=<?php print $model->pais_id?>" target="_self"><button type="button" class="btn btn-warning ">Actualizar Centros Médicos y Seguros</button></a>
      </div>
      <div>&nbsp;</div>
  </div>
  <?php } ?>


  <?php if (Yii::$app->session->hasFlash('success')): ?>

      <div class="alert alert-success alert-dismissable">

        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

         <?= Yii::$app->session->getFlash('success') ?>

      </div>

  <?php endif; ?>
    <?php if(Yii::$app->session->hasFlash('exception')): ?>

        <div class="alert alert-danger alert-dismissable">

            <?php echo Yii::$app->session->getFlash('exception'); ?>

        </div>

        <?php endif; ?>

<?php





$form = ActiveForm::begin([

    'options' => ['enctype' => 'multipart/form-data'],
    'method' => 'post',
    'id' => 'form-usuarios-createdoctor'

]);

?>


<div class="row">

  <div class="col-md-12 col-sm-12">

    <?= $form->errorSummary($model); ?>


  </div>

</div>

<div class="col-md-3 col-sm-6">

        <?= $form->field($modeldoctores, 'doctor_numero_colegialo')->textInput(['maxlength' => true,'class' => 'form-control','placeholder' => 'Ingrese su Número'])->label('N° de Colegiado <span class="required">*</span>') ?>

      </div>

<div class="col-md-3 col-sm-6">

    <?php echo $form->field($model, 'usuario_identificacion')->textInput([ 'class' => 'form-control', 'placeholder' => 'Cédula o Pasaporte'])->label('Cédula o pasaporte');

    ?>

</div>

  <div class="col-md-3 col-sm-6">
              <?php echo $form->field($model, 'usuario_genero')
                        ->radioList( Yii::$app->params['genero'],['inline'=>true])->label('Sexo');
                ?>
           </div>
      <br><br><br><br>

<div class="row">

  <div class="col-md-12">



      <div class="col-md-3 col-sm-6">

        <?= $form->field($model, 'usuario_nombre')->textInput(['maxlength' => true,'class' => 'form-control','placeholder' => 'Ingrese un Nombre'])->label('Nombre <span class="required">*</span>') ?>

      </div>

      <div class="col-md-3 col-sm-6">

        <?= $form->field($model, 'usuario_apellido')->textInput(['maxlength' => true,'placeholder' => 'Ingrese un Apellido'])->label('Apellido <span class="required">*</span>') ?>

      </div>

    <div class="col-md-3">

      <?= $form->field($model, 'usuario_email')->textInput(['placeholder' => 'E-mail'])->label('E-mail <span class="required">*</span>') ?>

    </div>

    <div class="col-md-3">

      <?= $form->field($model, 'usuario_email_confirmacion')->textInput(['placeholder' => 'E-mail'])->label(' Repetir E-mail <span class="required">*</span>') ?>

    </div>


    <?php

    if(!$model->isNewRecord){

      $model->usuario_password = "";

    }

    ?>

    <div class="col-md-3">

      <?= $form->field($model, 'usuario_password')->passwordInput(['placeholder' => 'Contraseña'])->label('Contraseña <span class="required">*</span>') ?>

    </div>

    <div class="col-md-3">

      <?= $form->field($model, 'usuario_password_confirmacion')->passwordInput(['placeholder' => 'Contraseña'])->label(' Repetir Contraseña <span class="required">*</span>') ?>

    </div>


  <div class="col-md-3 col-sm-6">

      <?= $form->field($model, 'usuario_telefono_1')->textInput(['placeholder' => 'Ingrese teléfono'])->label('Teléfono celular <span class="required">*</span>') ?>

  </div>

    <div class="col-md-3">

                 <span class="text-info small"><h4><strong>Tamaño: 600px ancho. 400px alto</strong> </h4></span>

           

                <?php echo $form->field($modeldoctores, 'doctor_imagen')->fileInput(['class'=>'col-md-12 btn btn-info']); ?>

                <div class="thumbnail">

                  <?php

                      $rutaImg1 = "#";
                      $classImg1 = "hidden";

                      if(!$modeldoctores->isNewRecord){

                        $rutaImg1 = Url::base(true).'/themes/tuagendamedica/resources/images/doctores/'.$modeldoctores->doctor_imagen;

                        $classImg1 = "";

                    }
                    if(isset($_REQUEST["code"])){
                        $rutaImg1 = Url::base(true).'/themes/tuagendamedica/resources/images/doctores/'.$modeldoctores->doctor_imagen;

                    }


                    if(empty($modeldoctores->doctor_imagen)) $rutaImg1 = "";

                  ?>

                  <img id="preview_doctores-doctor_imagen" src="<?php print $rutaImg1; ?>" alt="<?php print $modeldoctores->doctor_imagen?>" title="<?php print $modeldoctores->doctor_image?>" class="<?php print $classImg1; ?>" width="90" height="75" />

                </div>
            </div>
  </div>
</div>
  <div class="col-sm-3">

                      
<?php

   $is = 0;

      if(!$model->isNewRecord){

   $is = 1;

 }



?>



<script>

     $is = <?php  print $is ?> ;

</script> 

            <?php
              //muestro los pasies y aparte hago la configuracion para los estados

     $items = ArrayHelper::map(Paises::find()->where(['pais_estatus' => 1])
                                             ->orderBy(['(pais_nombre)' => SORT_ASC])
                                             ->all(), 'pais_id', 'pais_nombre');

            echo $form->field($model, 'pais_id')->dropDownList(
                 $items,['prompt'=>'Seleccione...',])->label('País <span class="required">*</span>');

              ?>

        </div>
        <div class="col-md-12 ">
          <div>&nbsp;</div>
        </div>
        <div class="col-md-12 ">
            <?php 
                    
        $items = ArrayHelper::map(Especialidades::find()->where(["especialidad_estatus"=>"1"])->all(), 'especialidad_nombre', 'especialidad_nombre'); 
          
          echo $form->field($modeldoctores, 'doctor_especialidades')->widget(Select2::classname(), [
                                    'data' => $items,
                                    'language' => 'es',
                                    'options' => ['placeholder' => 'Especialidad'],
                                    'pluginOptions' => [
                                            'allowClear' => true
                                    ],
                            ])->label('Especialidad');
                            
                            ?>


  </div>

        <div class="col-md-12">

          <?=

        $form->field($modeldoctores, 'doctor_resumen_academico')->widget(\yii\redactor\widgets\Redactor::className(), [

            'clientOptions' => [
                'imageUpload' => ['/cmsposts/upload'],
                'fileUpload' => ['/cmsposts/uploadfile'],
                'lang' => 'es',
                'plugins' => ['clips', 'fontcolor','imagemanager','video','table','fontfamily', 'filemanager']

            ]
        ])

          ?>

        </div>
        <div class="col-md-12">

          <?=

        $form->field($modeldoctores, 'doctor_logros')->widget(\yii\redactor\widgets\Redactor::className(), [

            'clientOptions' => [

                'imageUpload' => ['/cmsposts/upload'],
                'fileUpload' => ['/cmsposts/uploadfile'],
                'lang' => 'es',
                'plugins' => ['clips', 'fontcolor','imagemanager','video','table','fontfamily', 'filemanager']
            ]
        ])

          ?>

        </div>
        <div class="row">
          <div class="col-md-12">
            <div>&nbsp;</div>

       <?= Html::a(

          $model->isNewRecord ? 'Crear' : 'Actualizar',
          $model->isNewRecord ? ['usuarios/createdoctor'] : ['usuarios/updatedoctor','id'=>$model->usuario_id],

          [
              'data' => [
                  'method' => 'post',
                ],

            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'

            ])?>
      </div>

<?php if(Yii::$app->user->identity->usuario_perfil=='doctor' OR Yii::$app->user->identity->usuario_perfil=='administrador') {?>

<div class="col-md-6 ">

  <div>&nbsp;</div>

</div>

<?php

  

}  ?>

</div>

<?php ActiveForm::end(); ?>