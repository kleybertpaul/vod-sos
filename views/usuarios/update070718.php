<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */

$this->title = 'Actualizar Usuario';
//$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
	<div class="box-body">
	<div class="title-bmo-10"> <?= Html::encode($this->title) ?></div>
	<hr class="hr-bmo-10 ">
	
	<?php

	
	switch ($usuario){
		case 'doctor':
			print $this->render('_formdoctor', ['model' => $model, 'modeldoctores' => $modeldoctores]);
	
		break;
		case 'administrador':

			print  $this->render('_form', ['model' => $model]) ;
		break;
	}?>

		</div>
</div>
	


