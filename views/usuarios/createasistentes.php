<?php

use yii\helpers\Html;

$session = Yii::$app->session;
$sessionUsuarioNombre = $session['sessionSelectDoctor']['usuario_nombre']." ".$session['sessionSelectDoctor']['usuario_apellido'];

$this->title = Yii::t('app', 'Agregar asistente para '.$sessionUsuarioNombre);

$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-primary">
	<div class="box-body">
	<div class="title-bmo-10"> <?= Html::encode($this->title) ?></div>
	<hr class="hr-bmo-10 ">
	<?php
		print $this->render('_formasistentes', ['model' => $model]);
	?>
	</div>
</div>