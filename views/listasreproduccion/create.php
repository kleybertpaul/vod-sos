<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Listasreproduccion */

$this->title = Yii::t('app', 'Create Listasreproduccion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Listasreproduccions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listasreproduccion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
