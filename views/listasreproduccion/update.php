<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Listasreproduccion */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Listasreproduccion',
]) . $model->lista_reproduccion_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Listasreproduccions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lista_reproduccion_id, 'url' => ['view', 'id' => $model->lista_reproduccion_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="listasreproduccion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
