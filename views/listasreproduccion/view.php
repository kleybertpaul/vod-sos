<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Listasreproduccion */

$this->title = $model->lista_reproduccion_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Listasreproduccions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listasreproduccion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->lista_reproduccion_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->lista_reproduccion_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lista_reproduccion_id',
            'usuario_id',
            'lista_reproduccion_nombre',
        ],
    ]) ?>

</div>
