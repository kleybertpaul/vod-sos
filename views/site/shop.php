  <?php

use yii\helpers\Url;
$this->title = Yii::$app->params['panelClienteNombre'];

$theme = $this->theme;

if(!isset($categoria)) {
  $categoria = "";
}
if(!isset($subcategoria)) {
  $subcategoria = "";

} else {
   $categoria =$subcategoria->categoria->categoria_nombre;
   $subcategoria = $subcategoria->subcategoria_nombre;
}

?>  
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="product-listing grid-type">
            <div class="inner-listing">
              <div class="alert alert-success  fade out" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo \Yii::t('app', 'Success!'); ?> </strong>
                    <?php echo \Yii::t('app', 'Product have added to your list.'); ?>
              </div>
                <h3 class="">
                  <?php print $categoria."-". $subcategoria; ?>
                </h3>
                <div class="row">
                   <?php
                          foreach($modelShoptodos  as $key => $val) {  ?>

                <div class="col-md-2 item-width mb-30" style="margin-bottom:30px;" >
                  <div class="product-item">
                    <div class="product-image" style="background: #ffffff none repeat scroll 0 0 !important; "> 
                      <a href="productodetalle?lang=<?php print $lang; ?>&idproducto=<?php print $val->producto_id?>"> 
                        <img src="<?php print $theme->getUrl('resources/images/productos/'.$val->producto_imagen_pequeno);?>"  style="max-width:100%;"> 
                      </a>
                      <div class="product-detail-inner">
                        <div class="detail-inner-left align-center">
                          <ul>
                            <li class="pro-cart-icon" >

                     <button  onclick='agregarCarrito(<?php print  $val->producto_id;?>,
                                      "<?php print  addslashes($val->producto_titulo);?>",
                                      "\"<?php print  $val->producto_imagen_pequeno;?>\"",
                                      "\"<?php print  $val->producto_precio;?>\"",
                                      "\"<?php print  $val->producto_precio;?>\"")' title="<?php echo \Yii::t('app', 'Add to Cart'); ?>"><i class="fa fa-shopping-basket"></i></button>
                            </li>
                            <li class="pro-quick-view-icon"><a class="product-item-name" href="productodetalle?lang=<?php print $lang; ?>&idproducto=<?php print $val->producto_id?>" title="quick-view"><i class="fa fa-eye"></i></a></li>
                          </ul>
                          </div>
                      </div>
                    </div>
                    <div class="product-item-details">
                        <p style="font-size: 7px; margin-bottom: 0px;"  ><?php print $val->subcategoria->subcategoria_nombre;?></p>
                      <div class="product-item-name"> 
                        <a href="productodetalle?lang=<?php print $lang; ?>&idproducto=<?php print $val->producto_id?>"><?php print $val->producto_titulo;?></a> 
                      </div>
                      <div class="price-box"> 
                        <span class="price"><?php echo \Yii::t('app', '$'); ?><?php print $val->producto_precio;?></span> 
                      </div>
                      <div class="product-detail-inner">
                        <div class="detail-inner-left">
                          <ul>
                            <li class="pro-cart-icon">
              <button  onclick='agregarCarrito(<?php print  $val->producto_id;?>,
                                      "<?php print  addslashes($val->producto_titulo);?>",
                                      "\"<?php print  $val->producto_imagen_pequeno;?>\"",
                                      "\"<?php print  $val->producto_precio;?>\"",
                                      "\"<?php print  $val->producto_precio;?>\"")' title="Add to Cart"><i class="fa fa-shopping-basket"></i></button>
                            </li>
                            <li class="pro-quick-view-icon"><a title="quick-view" href="productodetalle?lang=<?php print $lang; ?>&idproducto=<?php print $val->producto_id?>" class="popup-with-product"><i class="fa fa-eye"></i></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php 
                } ?> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
