
<?php

use yii\helpers\Html;
use app\models\TransaccionesCompras;

$usuario = Yii::$app->user->identity->usuario_id;
$detalle =  \Yii::t('app', 'detalle');
$modelTransaccionesCompras = TransaccionesCompras::find()->Where(['users_usuario_id' => $usuario ])
        ->orderBy(['transaccion_compra_id' => SORT_DESC])->all();

?>
  <section class="checkout-section ptb-70">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <div class="account-sidebar account-tab mb-sm-30">
            <div class="dark-bg tab-title-bg">
              <div class="heading-part">
                <div class="sub-title"><span></span><?php echo \Yii::t('app', 'My Account'); ?></div>
              </div>
            </div>
            <div class="account-tab-inner">
              <ul class="account-tab-stap">
                <li id="step2"> <a href="javascript:void(0)"><?php echo \Yii::t('app', 'orders'); ?><i class="fa fa-angle-right"></i> </a> </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-9">
          <div id="data-step1" class="account-content" data-temp="tabdata">
            <div class="row">
              <div class="col-12">
                <div class="heading-part heading-bg mb-30">
                  <h2 class="heading m-0"><?php echo \Yii::t('app', 'accountDas'); ?></h2>
                </div>
              </div>
            </div>
            <div class="mb-30">
              <div class="row">
                <div class="col-12">
                  <div class="heading-part">
                    <h3 class="sub-heading"><?php echo \Yii::t('app', 'hello'); ?></h3>
                  </div>
                  <p><?php echo \Yii::t('app', 'message_init_account'); ?></p>
                </div>
              </div>
            </div>
          </div>
          <div id="data-step2" class="account-content" data-temp="tabdata" style="display: none;">
            <div class="row">
              <div class="col-12">
                <div class="heading-part heading-bg mb-30">
                  <h2 class="heading m-0"><?php echo \Yii::t('app', 'orders'); ?></h2>
                </div>
              </div>
            </div>
            <div class="m-0">
              <div class="row">
                <div class="col-md-12">
                  <div class="cart-total-table address-box commun-table">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>
                               <?php echo \Yii::t('app', 'fechaHora'); ?>
                            </th>
                            <th>
                               <?php echo \Yii::t('app', 'total'); ?>
                            </th>
                            <th>
                               <?php echo \Yii::t('app', 'opciones'); ?>
                            </th>
                          </tr>
                        </thead>
                        <tbody>

                          <?php 
                              foreach ($modelTransaccionesCompras as $key => $value) {
                          ?>

                              <tr>
                                <td>
                                    <?php print date('d-m-Y h:i A',strtotime($value["transaccion_compra_fecha"])); ?>
                                </td>
                                <td>
                                    <?php print $value["transaccion_compra_total_pago"]; ?>
                                </td>
                                <td>
                                    <?php

                                      print Html::beginForm(['/orderdetails?lang='.$_GET['lang']], 'post');
                                      print Html::hiddenInput('transaccion', $value["transaccion_compra_id"]);
                                      print Html::submitButton(
                                          $detalle,
                                          ['class' => 'btn btn-default btn-flat']
                                          );
                                      print Html::endForm();
                                  ?>
                                </td>
                              </tr>

                          <?php     
                              }
                          ?> 
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>