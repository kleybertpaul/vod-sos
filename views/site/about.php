  <section class="ptb-70">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="about-detail mt-40">
            <div class="row">
              <div class="col-12">
                <div class="heading-part line-bottom mb-30">
                  <h2 class="main_title heading"><span><?php echo \Yii::t('app', 'Who We Are'); ?></span></h2>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="heading-part-desc align_left center-md">
                  <h2 class="heading">"<?php echo \Yii::t('app', 'aboutText'); ?>"</h2>
                </div>
                <p><?php echo \Yii::t('app', 'aboutDES'); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
 