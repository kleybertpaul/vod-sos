
<section>
<br>
<br>
<br>
<?php if(($msg=Yii::$app->session->getAllflashes())!=null):?>
<?php foreach($msg as $type=>$message):?>
<div class="alert alert-<?php echo $type?>" style="background-color: black">
<button type="button" class="close " data-dismiss="alert"> &times;</button>
<h3><?php echo ucfirst ($type) ?>!</h3>
<?php echo $message ?>

</div>
<?php endforeach;?>

<?php endif;?>


</section>


<section class="mbr-section form1 cid-rnozJmQn6D" id="form1-6">
  <br>
  <br>
  <br>
  <br>

    <div class="container">
        <div class="row justify-content-center">
            <div class="title col-12 col-lg-8">
                <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
                    Registro de usuario
                </h2>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center mb-auto">
            <div class="media-container-column col-lg-8" data-form-type="formoid">
                
                <form action="FormularioRegistro" method="POST" class="mbr-form form-with-styler" data-form-title="Mobirise Form"><input type="hidden" name="email" data-form-email="true" value="">
                    <div class="row row-sm-offset">
                        <div hidden="hidden" data-form-alert="" class="alert alert-success col-12">Thanks for filling out the form!</div>
                        <div hidden="hidden" data-form-alert-danger="" class="alert alert-danger col-12">
                        </div>
                    </div>
                    <div class="dragArea row row-sm-offset">
                        <div class="col-md-4  form-group" data-for="name">
                            <label for="name-form1-6" class="form-control-label mbr-fonts-style display-7">Nombre</label>
                            <input type="text" name="name" data-form-field="Name"  class="form-control display-7" id="name-form1-6" minlength="3" maxlength="15" >
                            
                        </div>

                        <div class="col-md-4 form-group " data-for="apellido">
                            <label for="apellido-form1-6" class="form-control-label mbr-fonts-style display-7">Apellido</label>
                            <input type="text" name="apellido" data-form-field="Apellido" required="required" class="form-control display-7" id="apellido-form1-6" minlength="0" maxlength="15" >
                            
                        </div>

                        <div class="col-md-4 form-group " data-for="fechanacimiento">
                            <label for="fechanacimiento-form1-6" class="form-control-label mbr-fonts-style display-7">Fecha de Nacimiento</label>
                            <input type="date" name="fechanacimiento" data-form-field="Fechanacimiento" required="required" class="form-control display-7" id="fechanacimiento-form1-6">

                        </div>



                      <div class="col-md-12  form-group" data-for="pais">
                          <label for="pais-form1-6" class="form-control-label mbr-fonts-style display-7">Pais de Nacimiento</label>
                          <select class="form-control" name="pais" id="pais-form1-6">



                        <?php   foreach($modelpais  as $key => $val) { ?>

                             <option value="<?php print $val->pais_id; ?>"><?php print $val->pais_nombre; ?></option>



                         <?php    } ?>





                          </select>
                      </div>



                        <div class="col-md-12  form-group" data-for="email">
                            <label for="email-form1-6" class="form-control-label mbr-fonts-style display-7">Email</label>
                            <input type="email" name="email" data-form-field="Email" required="required" class="form-control display-7" id="email-form1-6" minlength="5" maxlength="30">
                        </div>

                        <div class="col-md-12  form-group" data-for="confirmacionemail">
                            <label for="email-form1-6" class="form-control-label mbr-fonts-style display-7"> Confirmacion de Email</label>
                            <input type="email" name="confirmacionemail" data-form-field="Confirmacionemail" required="required" class="form-control display-7" id="email-form1-7" minlength="5" maxlength="30">
                        </div>
                        <div data-for="password" class="col-md-6  form-group">
                            <label for="password-form1-6" class="form-control-label mbr-fonts-style display-7">Contraseña</label>
                            <input type="password" name="password" data-form-field="Password" class="form-control display-7" id="password-form1-6" minlength="5" maxlength="40"  required="required">
                        </div>

                        <div data-for="confirmacionpassword" class="col-md-6  form-group">
                            <label for="password-form1-6" class="form-control-label mbr-fonts-style display-7">Confirmar contraseña</label>
                            <input type="password" name="confirmacionpassword" data-form-field="Confirmacionpassword" class="form-control display-7" id="password-form1-7"  required="required" >
                        </div>

                        <div class="col-md-12 input-group-btn align-center">
                            <button type="submit" class="btn btn-black btn-form display-4">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
