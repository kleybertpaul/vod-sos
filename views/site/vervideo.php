<link rel="stylesheet" href="https://cdn.plyr.io/3.5.1/plyr.css"/>


<section>
  <div class="container">
    <div class="row mt-3 justify-content-center">
      <div class="col-12">
        <br>
        <br>
        <br>
        <br>

        <video class='js-player' poster="thumbnail.jpg" controls playsinline>
          <!--  <source src="img/video.mp4" type="video/mp4"> -->
              <!-- Calidad -->
            <source src="themes/sostelemedicinavod/videos/video_576p.mp4" size="576" type="video/mp4">
            <source src="themes/sostelemedicinavod/videos/video_720p.mp4" size="720" type="video/mp4">
            <source src="themes/sostelemedicinavod/videos/video_1080p.mp4" size="1080" type="video/mp4">
        </video>

      </div>
<div class="row mt-3 justify-content-center">
      <div class="col">
        <div class="jumbotron" style="background:#000; color:#FFF;">
          <h3 class="display-3 mb-3">Titulo del video</h3>
          <p class="lead mb-4">Tomando en cuenta que un seminario investigativo
            es un grupo de aprendizaje activo en el que quienes participan no
            reciben la información elaborada,
            sino que, indagan en un tema</p>

            <a href="#" class="btn btn-primary btn-lg"><img src="" alt="">Agregar a lista de reproducción</a>
            <a href="#" class="btn btn-success btn-lg"><img src="" alt="">Descargar</a>
        </div>
      </div>



    </div>
</div>
  </div>

</section>


<script src="https://cdn.plyr.io/3.5.1/plyr.js"></script>
<script>/*<![CDATA[*/const players = Array.from(document.querySelectorAll('.js-player')).map(player => new Plyr(player));/*]]>*/</script>
