<section>
<br>
<br>
<br>

<?php if(($msg=Yii::$app->session->getAllflashes())!=null):?>
<?php foreach($msg as $type=>$message):?>
<div class="alert alert-<?php echo $type?>" style="background-color: black">
<button type="button"  class="close" data-dismiss="alert"> &times;</button>
<h3><?php echo ucfirst ($type) ?>!</h3>
<?php echo $message ?>

</div>
<?php endforeach;?>

<?php endif;?>


</section>


<section class="engine"><a href="">show to develop own website</a></section><section class="carousel slide cid-rnhKGcmKot" data-interval="false" id="slider1-4">

    <div class="full-screen"><div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="false" data-interval="false"><ol class="carousel-indicators"><li data-app-prevent-settings="" data-target="#slider1-4" class=" active" data-slide-to="0"></li><li data-app-prevent-settings="" data-target="#slider1-4" data-slide-to="1"></li><li data-app-prevent-settings="" data-target="#slider1-4" data-slide-to="2"></li></ol><div class="carousel-inner" role="listbox"><div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false" style="background-image: url(themes/sostelemedicinavod/img/background1.jpg);"><div class="container container-slide"><div class="image_wrapper"><div class="mbr-overlay"></div><img src="themes/sostelemedicinavod/img/background1.jpg"><div class="carousel-caption justify-content-center"><div class="col-10 align-center"><h2 class="mbr-fonts-style display-1">CARRUSEL DE IMÁGENES A PANTALLA COMPLETA</h2><p class="lead mbr-text mbr-fonts-style display-5">Elija entre la gran selección de bloques prediseñados más recientes: jumbotrons,  hero images, desplazamiento con paralaje, fondos con videos, menú de hamburguesa, encabezado fijo y más.</p></div></div></div></div></div><div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(themes/sostelemedicinavod/img/background2.jpg);"><div class="container container-slide"><div class="image_wrapper"><div class="mbr-overlay"></div><img src="themes/sostelemedicinavod/img/background2.jpg"><div class="carousel-caption justify-content-center"><div class="col-10 align-left"><h2 class="mbr-fonts-style display-1">DIAPOSITIVA DE VIDEO</h2><p class="lead mbr-text mbr-fonts-style display-5">Diapositiva con fondo de video de youtube y superposición con color. El título y el texto están alineados a la izquierda.</p></div></div></div></div></div><div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(themes/sostelemedicinavod/img/background3.jpg);"><div class="container container-slide"><div class="image_wrapper"><div class="mbr-overlay"></div><img src="themes/sostelemedicinavod/img/background3.jpg"><div class="carousel-caption justify-content-center"><div class="col-10 align-right"><h2 class="mbr-fonts-style display-1">DIAPOSITIVA DE IMAGEN</h2><p class="lead mbr-text mbr-fonts-style display-5">Elija entre la gran selección de bloques prediseñados más recientes: jumbotrons,  hero images, desplazamiento con paralaje, fondos con videos, menú de hamburguesa, encabezado fijo y más.</p></div></div></div></div></div></div><a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#slider1-4"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span></a><a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#slider1-4"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span></a></div></div>

</section>




<section class="mbr-gallery mbr-slider-carousel cid-rnhK9hq3bS" id="gallery1-3">


    <div class="container">
        <div><!-- Filter --><!-- Gallery -->
          <div class="mbr-gallery-row">
            <div class="mbr-gallery-layout-default">
              <div>
                <div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="0" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background1.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="1" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background2.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Creativa">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="2" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background3.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animada">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="3" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background4.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="4" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background5.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="5" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background6.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="6" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background7.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                  <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animada">
                    <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="7" data-toggle="modal">
                      <img src="themes/sostelemedicinavod/img/background8.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div><!-- Lightbox -->
          <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery1-3">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="themes/sostelemedicinavod/img/background1.png" alt="" title="">
                    </div>
                    <div class="carousel-item">
                      <img src="themes/sostelemedicinavod/img/background2.png" alt="" title="">
                    </div>
                    <div class="carousel-item">
                      <img src="themes/sostelemedicinavod/img/background3.png" alt="" title="">
                    </div>
                    <div class="carousel-item">
                      <img src="themes/sostelemedicinavod/img/background4.png" alt="" title="">
                    </div>
                    <div class="carousel-item">
                      <img src="themes/sostelemedicinavod/img/background5.png" alt="" title="">
                    </div>
                    <div class="carousel-item">
                      <img src="themes/sostelemedicinavod/img/background6.png" alt="" title="">
                    </div>
                    <div class="carousel-item">
                      <img src="themes/sostelemedicinavod/img/background7.png" alt="" title="">
                    </div>
                    <div class="carousel-item">
                      <img src="themes/sostelemedicinavod/img/background8.png" alt="" title="">
                    </div>
                  </div>
                  <a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery1-3"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a>
                  <a class="carousel-control carousel-control-next" role="button" data-slide="next" href="#lb-gallery1-3"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a><a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</section>

<section class="mbr-gallery mbr-slider-carousel cid-rnhLRVuvA8" id="gallery1-5">



  <div class="container">
      <div><!-- Filter --><!-- Gallery -->
        <div class="mbr-gallery-row">
          <div class="mbr-gallery-layout-default">
            <div>
              <div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="0" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background1.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="1" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background2.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Creativa">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="2" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background3.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animada">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="3" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background4.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="4" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background5.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="5" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background6.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="6" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background7.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animada">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="7" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background8.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div><!-- Lightbox -->
        <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery1-3">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="themes/sostelemedicinavod/img/background1.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background2.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background3.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background4.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background5.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background6.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background7.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background8.png" alt="" title="">
                  </div>
                </div>
                <a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery1-3"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a>
                <a class="carousel-control carousel-control-next" role="button" data-slide="next" href="#lb-gallery1-3"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a><a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<section class="mbr-gallery mbr-slider-carousel cid-rnhLTqrf6H" id="gallery1-6">



  <div class="container">
      <div><!-- Filter --><!-- Gallery -->
        <div class="mbr-gallery-row">
          <div class="mbr-gallery-layout-default">
            <div>
              <div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="0" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background1.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="1" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background2.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Creativa">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="2" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background3.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animada">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="3" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background4.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="4" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background5.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Increíble">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="5" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background6.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="6" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background7.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animada">
                  <div class="hover-imagen" href="#lb-gallery1-3" data-slide-to="7" data-toggle="modal">
                    <img src="themes/sostelemedicinavod/img/background8.png" alt="" title=""><span class="mbr-gallery-title mbr-fonts-style display-7">Título del video</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div><!-- Lightbox -->
        <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery1-3">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="themes/sostelemedicinavod/img/background1.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background2.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background3.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background4.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background5.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background6.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background7.png" alt="" title="">
                  </div>
                  <div class="carousel-item">
                    <img src="themes/sostelemedicinavod/img/background8.png" alt="" title="">
                  </div>
                </div>
                <a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery1-3"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a>
                <a class="carousel-control carousel-control-next" role="button" data-slide="next" href="#lb-gallery1-3"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a><a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
