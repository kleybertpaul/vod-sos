<script src="https://www.google.com/recaptcha/api.js" async defer></script>
  
  <section class="pt-70">
    <div class="container">
      <div class="row">
      <?php if (Yii::$app->session->hasFlash('success')): ?>
              <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
               <?= Yii::$app->session->getFlash('success') ?>
              </div>
          <?php endif; ?>
        <div class="col-12">
          <div class="map">
            <div class="map-part">
              <div id="map" class="map-inner-part"></div>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3592.067172296591!2d-80.31536438550319!3d25.80135791325619!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9b984d931f15b%3A0x40c4f8cb1a721482!2s2930+NW+72nd+Ave+FL+3312%2C+Miami%2C+FL+33122%2C+EE.+UU.!5e0!3m2!1ses-419!2sve!4v1553022598990" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>    
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-70 client-main align-center">
    <div class="container">
      <div class="contact-info">
        <div class="row m-0">
          <div class="col-md-4 p-0">
            <div class="contact-box">
              <div class="contact-icon contact-phone-icon"></div>
              <span><b><?php echo \Yii::t('app', 'Tel'); ?></b></span>
              <p>+ 1 (786) 6160509</p>
            </div>
          </div>
          <div class="col-md-4 p-0">
            <div class="contact-box">
              <div class="contact-icon contact-mail-icon"></div>
              <span><b><?php echo \Yii::t('app', 'Mail'); ?></b></span>
              <p>sales@nextouchonline.com </p>
            </div>
          </div>
          <div class="col-md-4 p-0">
            <div class="contact-box">
              <div class="contact-icon contact-open-icon"></div>
              <span><b><?php echo \Yii::t('app', 'Open'); ?></b></span>
              <p><?php echo \Yii::t('app', 'Mon – Sat: 9:00 – 18:00'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="ptb-70">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading-part line-bottom mb-30">
            <h2 class="main_title  heading"><span><?php echo \Yii::t('app', 'Leave a message!'); ?></span></h2>
          </div>
        </div>
      </div>
      <div class="main-form">
        <form method="POST" name="contactform">
          <div class="row">
            <div class="col-md-4 mb-30">
              <input type="text" required placeholder="<?php echo \Yii::t('app', 'First Name'); ?>" name="name">
            </div>
            <div class="col-md-4 mb-30">
              <input type="email" required placeholder="<?php echo \Yii::t('app', 'Email address'); ?>" name="email">
            </div>
            <div class="col-md-4 mb-30">
              <input type="text" required placeholder="<?php echo \Yii::t('app', 'Website'); ?>" name="website">
            </div>
            <div class="col-12 mb-30">
              <textarea required placeholder="<?php echo \Yii::t('app', 'Message'); ?>" rows="3" cols="30" name="message"></textarea>
            </div>
            <div class= "col-md-12">
              
      <div class="g-recaptcha" data-sitekey="6LfGeJoUAAAAANJ70vc3aG3H3kORsAmBOZ9biplF"></div>
            </div>
            <div class="col-12">
              <div class="align-center">
                <button type="submit" name="submit" class="btn btn-color"><?php echo \Yii::t('app', 'Submit'); ?></button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>

  


