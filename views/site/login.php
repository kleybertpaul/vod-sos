<!--formulario donde se iniciara sesionn -->
<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

    <?php $form    = ActiveForm::begin(['id'=> 'login-form','layout'=> 'horizontal',]); ?>

  <!-- CONTAIN START -->
  <section class="checkout-section ptb-70">
    <div class="container">
      <div class="row">
        <div class="col-12">
        <?php if (Yii::$app->session->hasFlash('success')): ?>
              <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
               <?= Yii::$app->session->getFlash('success') ?>
              </div>
          <?php endif; ?>
          <?php if(Yii::$app->session->hasFlash('exception')): ?>

<div class="alert alert-danger alert-dismissable">

        <?php echo Yii::$app->session->getFlash('exception'); ?>

</div>
<?php endif; ?>
          <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 col-md-8 ">
              <form class="main-form full">
                <div class="row">
                  <div class="col-12 mb-20">
                    <div class="heading-part heading-bg">
                      <h2 class="heading"><?php echo \Yii::t('app', 'Customer Login'); ?></h2>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="main-form">
                     <?= $form->field($model, 'username')->textInput(['autofocus' => true,'maxlength' => true,'style'=>'font-size:15px;','class' => 'newsletter-box ']) ?>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="main-form">
                                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'style'=>'font-size:15px;','class' => 'newsletter-box']) ?>
                    </div>
                  </div>
                  <div class="col-12">
   
                         <?= Html::submitButton(\Yii::t('app', 'Submit'), ['class' => 'btn-color', 'name' => 'login-button']) ?>

                  </div>
                  <div class="col-12"> <a title="Forgot Password" class="forgot-password mtb-20" href="correo"><?php echo \Yii::t('app', 'Forgot your password?'); ?></a>
                    <hr>
                  </div>
                  <div class="col-12">
                    <div class="new-account align-center mt-20"> <span><?php echo \Yii::t('app', 'New to Nexttouch?'); ?></span> <a class="link" title="Register with Nexttouch" href="register"><?php echo \Yii::t('app', 'Create New Account'); ?></a> </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
   <?php ActiveForm::end(); ?>

