<?php
  use yii\helpers\Html;
  use kartik\widgets\ActiveForm;
  use yii\helpers\ArrayHelper;
  use kartik\widgets\ActiveField;
?>
<?php
        
        $form= ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'method'  => 'post',
        'id'      => 'form-usuarios-registro']);
        ?>

  <section class="checkout-section ptb-70">
    <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12">
                    <?= $form->errorSummary($model); ?>
                    <div>&nbsp;</div>
                </div>
        <div class="col-12">
          <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8 col-md-8 ">
              <form class="main-form full">
                <div class="row">
                  <div class="col-12 mb-20">
                    <div class="heading-part heading-bg">
                      <h2 class="heading"><?php echo \Yii::t('app', 'Create your account'); ?></h2>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="heading-part line-bottom ">
                      <h2 class="form-title heading"><?php echo \Yii::t('app', 'Your Personal Details'); ?></h2>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-box">
                                     
    <?= $form->field($model, 'usuario_nombre')->textInput(['maxlength' => true,'style'=>'font-size:15px;','class' => 'form-group','placeholder' =>  \Yii::t('app', 'First Name')]) ?>
  
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-box">
                      <?= $form->field($model, 'usuario_apellido')->textInput(['maxlength' => true,'style'=>'font-size:15px;','class' => 'form-group','placeholder' =>  \Yii::t('app', 'Last Name')]) ?>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-box">
                   <?= $form->field($model, 'usuario_email')->textInput(['maxlength' => true,'style'=>'font-size:15px;','class' => 'form-group','placeholder' =>  \Yii::t('app', 'Email address')]) ?>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-box">
                          <?= $form->field($model, 'usuario_telefono_1')->textInput(['maxlength' => true,'style'=>'font-size:15px;','class' => 'form-group','placeholder' =>  \Yii::t('app', 'Telephone')]) ?>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-box">
                          <?= $form->field($model, 'usuario_password')->passwordInput(['maxlength' => true,'style'=>'font-size:15px;','class' => 'form-group','placeholder' =>  \Yii::t('app', 'Password')]) ?>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-box">
                           <?= $form->field($model, 'usuario_password_confirmacion')->passwordInput(['maxlength' => true,'style'=>'font-size:15px;','class' => 'form-group','placeholder' =>  \Yii::t('app', 'Re-enter Password')]) ?>
                    </div>
                  </div>
                  <div class="col-12">

        <?= Html::submitButton(Yii::t('app', \Yii::t('app', 'Submit')) , 
        ['class' =>  'btn-color right-side', 'style'=> 'padding: 13px 15px']) ?>
   
                
                  </div>
                  <div class="col-12">
                    <hr>
                    <div class="new-account align-center mt-20"> <span><?php echo \Yii::t('app', 'Already have an account with us'); ?></span> <a class="link" title="Register with Ezone" href="login<?php echo $lang?>"><?php echo \Yii::t('app', 'Login Here'); ?></a> </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
         <?php ActiveForm::end(); ?>