<?php
use yii\helpers\Html;
use app\assets\sostelemedicinavodAsset;;
use app\controllers\SiteController;


sostelemedicinavodAsset::register($this);
$theme = $this->theme;

?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="themes/sostelemedicinavod/img/logo.png" type="image/x-icon">
  <meta name="description" content="">


<!-- HEADER -->
<section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">


  <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">


      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <div class="hamburger">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
          </div>


      </button>


      <div class="menu-logo">
          <div class="navbar-brand">
              <span class="navbar-logo">
                  <a href="https://mobirise.com">
                       <img src="themes/sostelemedicinavod/img/logo.png" alt="sostelemedicinavod" style="height: 3.8rem;">
                  </a>
              </span>
              <span class="navbar-caption-wrap">
                  <a class="navbar-caption text-white display-4" href="https://mobirise.com">
                      SOS TELEMEDICINA VOD
                  </a>
              </span>
          </div>
      </div>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <div class="navbar-buttons mbr-section-btn">
              <a class="btn btn-sm btn-primary display-4" href="#">
                  Inicia Sesion
              </a>
          </div>
      </div>
  </nav>


</section>






<!-- HEADER END-->

        <?php $this->head() ?>

      </head>
      <body>

        <?php $this->beginBody() ?>

        <main>

            <?php print $content; ?>

        </main>

        <?php $this->endBody() ?>


</body>
  <!-- FOOTER START -->
  <section class="cid-qTkAaeaxX5" id="footer1-2">





      <div class="container">
          <div class="media-container-row content text-white">
              <div class="col-12 col-md-3">
                  <div class="media-wrap">
                      <a href="https://mobirise.com/">
                          <img src="themes/sostelemedicinavod/img/logo.png" alt="sostelemedicinavod">
                      </a>
                  </div>
              </div>
              <div class="col-12 col-md-3 mbr-fonts-style display-7">
                  <h5 class="pb-3">
                      Address
                  </h5>
                  <p class="mbr-text">
                      1234 Street Name
                      <br>City, AA 99999
                  </p>
              </div>
              <div class="col-12 col-md-3 mbr-fonts-style display-7">
                  <h5 class="pb-3">
                      Contacto
                  </h5>
                  <p class="mbr-text">
                      Email: support@mobirise.com
                      <br>Phone: +1 (0) 000 0000 001
                      <br>Fax: +1 (0) 000 0000 002
                  </p>
              </div>
              <div class="col-12 col-md-3 mbr-fonts-style display-7">
                  <h5 class="pb-3">
                      Links
                  </h5>
                  <p class="mbr-text">
                      <a class="text-primary" href="https://mobirise.com/">Website builder</a>
                      <br><a class="text-primary" href="https://mobirise.com/mobirise-free-win.zip">Download for Windows</a>
                      <br><a class="text-primary" href="https://mobirise.com/mobirise-free-mac.zip">Download for Mac</a>
                  </p>
              </div>
          </div>
          <div class="footer-lower">
              <div class="media-container-row">
                  <div class="col-sm-12">
                      <hr>
                  </div>
              </div>
              <div class="media-container-row mbr-white">
                  <div class="col-sm-6 copyright">
                      <p class="mbr-text mbr-fonts-style display-7">
                          © Copyright 2019 sostelemedicinavod - All Rights Reserved
                      </p>
                  </div>
                  <div class="col-md-6">
                      <div class="social-list align-right">
                          <div class="soc-item">
                              <a href="https://twitter.com/sostelemedicina?lang=es" target="_blank">
                                  <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                              </a>
                          </div>
                          <div class="soc-item">
                              <a href="https://es-la.facebook.com/SOSTelemedicinaUCV/" target="_blank">
                                  <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                              </a>
                          </div>
                          <div class="soc-item">
                              <a href="https://www.youtube.com/user/TheSOStelemedicina" target="_blank">
                                  <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                              </a>
                          </div>
                          <div class="soc-item">
                              <a href="https://www.instagram.com/sostelemedicina/?hl=es-la" target="_blank">
                                  <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                              </a>
                          </div>
                          <div class="soc-item">
                              <a href="https://plus.google.com/+SOSTelemedicinaparaVenezuela" target="_blank">
                                  <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>


  <!-- FOOTER END -->



</html>
<?php $this->endPage() ?>
