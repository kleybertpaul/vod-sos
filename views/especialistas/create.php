<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Especialistas */

$this->title = Yii::t('app', 'Create Especialistas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Especialistas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="especialistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
