<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListareproduccionvideosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Listareproduccionvideos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listareproduccionvideos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Listareproduccionvideos'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'lista_reproduccion_video_id',
            'video_id',
            'lista_reproduccion_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
