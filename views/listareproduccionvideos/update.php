<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Listareproduccionvideos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Listareproduccionvideos',
]) . $model->lista_reproduccion_video_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Listareproduccionvideos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lista_reproduccion_video_id, 'url' => ['view', 'id' => $model->lista_reproduccion_video_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="listareproduccionvideos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
