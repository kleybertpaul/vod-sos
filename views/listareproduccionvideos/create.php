<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Listareproduccionvideos */

$this->title = Yii::t('app', 'Create Listareproduccionvideos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Listareproduccionvideos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listareproduccionvideos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
