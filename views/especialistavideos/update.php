<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Especialistavideos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Especialistavideos',
]) . $model->especialista_video_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Especialistavideos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->especialista_video_id, 'url' => ['view', 'id' => $model->especialista_video_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="especialistavideos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
