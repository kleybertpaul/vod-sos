<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Especialistavideos */

$this->title = Yii::t('app', 'Create Especialistavideos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Especialistavideos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="especialistavideos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
