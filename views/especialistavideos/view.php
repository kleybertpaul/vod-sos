<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Especialistavideos */

$this->title = $model->especialista_video_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Especialistavideos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="especialistavideos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->especialista_video_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->especialista_video_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'especialista_video_id',
            'video_id',
            'especialista_id',
        ],
    ]) ?>

</div>
