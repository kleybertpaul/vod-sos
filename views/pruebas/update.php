<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pruebas */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pruebas',
]) . $model->prueba_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pruebas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->prueba_id, 'url' => ['view', 'id' => $model->prueba_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pruebas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
