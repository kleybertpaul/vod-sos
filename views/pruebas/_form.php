<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pruebas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pruebas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'prueba_nombre')->textinput() ?>

    <?= $form->field($model, 'prueba_apellido')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
