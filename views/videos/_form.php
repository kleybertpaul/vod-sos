<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Videos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'video_nombre_archivo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_duracion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_formato')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'video_cantidad_reproducciones')->textInput() ?>

    <?= $form->field($model, 'video_activo')->textInput() ?>

    <?= $form->field($model, 'especialista_id')->textInput() ?>

    <?= $form->field($model, 'video_palabras_claves')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_favorito')->textInput() ?>

    <?= $form->field($model, 'video_tipo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_fecha_creacion')->textInput() ?>

    <?= $form->field($model, 'video_fecha_actualizacion')->textInput() ?>

    <?= $form->field($model, 'usuario_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
