<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VideosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'video_id') ?>

    <?= $form->field($model, 'video_nombre_archivo') ?>

    <?= $form->field($model, 'video_titulo') ?>

    <?= $form->field($model, 'video_duracion') ?>

    <?= $form->field($model, 'video_formato') ?>

    <?php // echo $form->field($model, 'video_descripcion') ?>

    <?php // echo $form->field($model, 'video_cantidad_reproducciones') ?>

    <?php // echo $form->field($model, 'video_activo') ?>

    <?php // echo $form->field($model, 'especialista_id') ?>

    <?php // echo $form->field($model, 'video_palabras_claves') ?>

    <?php // echo $form->field($model, 'video_favorito') ?>

    <?php // echo $form->field($model, 'video_tipo') ?>

    <?php // echo $form->field($model, 'video_fecha_creacion') ?>

    <?php // echo $form->field($model, 'video_fecha_actualizacion') ?>

    <?php // echo $form->field($model, 'usuario_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
