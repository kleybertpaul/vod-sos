<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Videoespecialidades */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Videoespecialidades',
]) . $model->video_especialidad_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Videoespecialidades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->video_especialidad_id, 'url' => ['view', 'id' => $model->video_especialidad_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="videoespecialidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
