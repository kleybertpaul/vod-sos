<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Videoespecialidades */

$this->title = Yii::t('app', 'Create Videoespecialidades');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Videoespecialidades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videoespecialidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
