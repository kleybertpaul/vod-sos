<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property integer $usuario_id
 * @property string $usuario_nombre
 * @property string $usuario_apellido
 * @property string $usuario_clave
 * @property string $usuario_correo
 * @property string $usuario_fecha_de_nacimiento
 * @property string $usuario_grado_de_instruccion
 * @property integer $pais_id
 * @property string $usuario_perfil
 *
 * @property ListasReproduccion[] $listasReproduccions
 * @property Paises $pais
 * @property Videos[] $videos
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'usuario_nombre', 'usuario_apellido', 'usuario_clave', 'usuario_correo', 'usuario_fecha_de_nacimiento', 'usuario_grado_de_instruccion', 'pais_id', 'usuario_perfil'], 'required'],
            [['usuario_id', 'pais_id'], 'integer'],
            [['usuario_fecha_de_nacimiento', 'usuario_grado_de_instruccion'], 'safe'],
            [['usuario_nombre', 'usuario_apellido', 'usuario_clave', 'usuario_correo', 'usuario_perfil'], 'string', 'max' => 100],
            [['pais_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::className(), 'targetAttribute' => ['pais_id' => 'pais_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usuario_id' => Yii::t('app', 'Usuario ID'),
            'usuario_nombre' => Yii::t('app', 'Usuario Nombre'),
            'usuario_apellido' => Yii::t('app', 'Usuario Apellido'),
            'usuario_clave' => Yii::t('app', 'Usuario Clave'),
            'usuario_correo' => Yii::t('app', 'Usuario Correo'),
            'usuario_fecha_de_nacimiento' => Yii::t('app', 'Usuario Fecha De Nacimiento'),
            'usuario_grado_de_instruccion' => Yii::t('app', 'Usuario Grado De Instruccion'),
            'pais_id' => Yii::t('app', 'Pais ID'),
            'usuario_perfil' => Yii::t('app', 'Usuario Perfil'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListasReproduccions()
    {
        return $this->hasMany(ListasReproduccion::className(), ['usuario_id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(Paises::className(), ['pais_id' => 'pais_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(Videos::className(), ['usuarios_usuario_id' => 'usuario_id']);
    }
}
