<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "videos".
 *
 * @property integer $video_id
 * @property string $video_nombre_archivo
 * @property string $video_titulo
 * @property string $video_duracion
 * @property string $video_formato
 * @property string $video_descripcion
 * @property integer $video_cantidad_reproducciones
 * @property integer $video_activo
 * @property integer $especialista_id
 * @property string $video_palabras_claves
 * @property integer $video_favorito
 * @property string $video_tipo
 * @property string $video_fecha_creacion
 * @property string $video_fecha_actualizacion
 * @property integer $usuario_id
 *
 * @property EspecialistaVideos[] $especialistaVideos
 * @property ListaReproduccionVideos[] $listaReproduccionVideos
 * @property VideoEspecialidades[] $videoEspecialidades
 * @property Especialistas $especialista
 * @property Usuarios $usuario
 */
class Videos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'videos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_nombre_archivo', 'video_titulo', 'video_duracion', 'video_formato', 'video_descripcion', 'especialista_id', 'video_palabras_claves', 'video_tipo', 'video_fecha_creacion', 'video_fecha_actualizacion', 'usuario_id'], 'required'],
            [['video_descripcion'], 'string'],
            [['video_cantidad_reproducciones', 'video_activo', 'especialista_id', 'video_favorito', 'usuario_id'], 'integer'],
            [['video_fecha_creacion', 'video_fecha_actualizacion'], 'safe'],
            [['video_nombre_archivo', 'video_tipo'], 'string', 'max' => 100],
            [['video_titulo', 'video_duracion', 'video_formato', 'video_palabras_claves'], 'string', 'max' => 45],
            [['especialista_id'], 'exist', 'skipOnError' => true, 'targetClass' => Especialistas::className(), 'targetAttribute' => ['especialista_id' => 'especialista_id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario_id' => 'usuario_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'video_id' => Yii::t('app', 'Video ID'),
            'video_nombre_archivo' => Yii::t('app', 'Video Nombre Archivo'),
            'video_titulo' => Yii::t('app', 'Video Titulo'),
            'video_duracion' => Yii::t('app', 'Video Duracion'),
            'video_formato' => Yii::t('app', 'Video Formato'),
            'video_descripcion' => Yii::t('app', 'Video Descripcion'),
            'video_cantidad_reproducciones' => Yii::t('app', 'Video Cantidad Reproducciones'),
            'video_activo' => Yii::t('app', 'Video Activo'),
            'especialista_id' => Yii::t('app', 'Especialista ID'),
            'video_palabras_claves' => Yii::t('app', 'Video Palabras Claves'),
            'video_favorito' => Yii::t('app', 'Video Favorito'),
            'video_tipo' => Yii::t('app', 'Video Tipo'),
            'video_fecha_creacion' => Yii::t('app', 'Video Fecha Creacion'),
            'video_fecha_actualizacion' => Yii::t('app', 'Video Fecha Actualizacion'),
            'usuario_id' => Yii::t('app', 'Usuario ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialistaVideos()
    {
        return $this->hasMany(EspecialistaVideos::className(), ['video_id' => 'video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListaReproduccionVideos()
    {
        return $this->hasMany(ListaReproduccionVideos::className(), ['video_id' => 'video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideoEspecialidades()
    {
        return $this->hasMany(VideoEspecialidades::className(), ['video_id' => 'video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialista()
    {
        return $this->hasOne(Especialistas::className(), ['especialista_id' => 'especialista_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['usuario_id' => 'usuario_id']);
    }
}
