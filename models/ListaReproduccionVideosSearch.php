<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Listareproduccionvideos;

/**
 * ListareproduccionvideosSearch represents the model behind the search form about `app\models\Listareproduccionvideos`.
 */
class ListareproduccionvideosSearch extends Listareproduccionvideos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lista_reproduccion_video_id', 'video_id', 'lista_reproduccion_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Listareproduccionvideos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lista_reproduccion_video_id' => $this->lista_reproduccion_video_id,
            'video_id' => $this->video_id,
            'lista_reproduccion_id' => $this->lista_reproduccion_id,
        ]);

        return $dataProvider;
    }
}
