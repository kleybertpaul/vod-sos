<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "listasreproduccion".
 *
 * @property integer $lista_reproduccion_id
 * @property integer $usuario_id
 * @property string $lista_reproduccion_nombre
 *
 * @property Listareproduccionvideos[] $listareproduccionvideos
 * @property Usuarios $usuario
 */
class Listasreproduccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'listasreproduccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'lista_reproduccion_nombre'], 'required'],
            [['usuario_id'], 'integer'],
            [['lista_reproduccion_nombre'], 'string', 'max' => 100],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario_id' => 'usuario_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lista_reproduccion_id' => Yii::t('app', 'Lista Reproduccion ID'),
            'usuario_id' => Yii::t('app', 'Usuario ID'),
            'lista_reproduccion_nombre' => Yii::t('app', 'Lista Reproduccion Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListareproduccionvideos()
    {
        return $this->hasMany(Listareproduccionvideos::className(), ['lista_reproduccion_id' => 'lista_reproduccion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['usuario_id' => 'usuario_id']);
    }
}
