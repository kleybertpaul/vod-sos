<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $usuario_id
 * @property string $usuario_nombre
 * @property string $usuario_apellido
 * @property string $usuario_login
 * @property string $usuario_password
 * @property string $usuario_fecha_creacion
 * @property string $usuario_fecha_modificacion
 * @property string $usuario_fecha_ultimo_acceso
 * @property string $usuario_email
 * @property string $usuario_telefono_1
 * @property string $usuario_telefono_2
 * @property string $usuario_direccion
 * @property integer $usuario_activo
 * @property integer $usuario_online
 * @property string $usuario_perfil
 * @property string $usuario_imagen_1
 * @property string $usuario_estado
 * @property string $usuario_mensaje_publico
 * @property string $usuario_genero
 * @property string $usuario_fecha_nacimiento
 * @property string $authKey
 * @property string $accessToken
 * @property string $usuario_fecha_codigo
 * @property integer $usuario_codigo
 *
 * @property CmsBanners[] $cmsBanners
 * @property CmsGeneralPages[] $cmsGeneralPages
 * @property Productos[] $productos
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_nombre', 'usuario_apellido', 'usuario_login', 'usuario_password', 'usuario_email', 'usuario_perfil'], 'required'],
            [['usuario_password', 'usuario_email', 'usuario_direccion', 'usuario_imagen_1', 'authKey', 'accessToken'], 'string'],
            [['usuario_fecha_creacion', 'usuario_fecha_modificacion', 'usuario_fecha_ultimo_acceso', 'usuario_fecha_nacimiento', 'usuario_fecha_codigo'], 'safe'],
            [['usuario_activo', 'usuario_online', 'usuario_codigo'], 'integer'],
            [['usuario_nombre', 'usuario_apellido', 'usuario_login', 'usuario_perfil', 'usuario_genero'], 'string', 'max' => 45],
            [['usuario_telefono_1', 'usuario_telefono_2'], 'string', 'max' => 15],
            [['usuario_estado'], 'string', 'max' => 20],
            [['usuario_mensaje_publico'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usuario_id' => 'Usuario ID',
            'usuario_nombre' => 'Usuario Nombre',
            'usuario_apellido' => 'Usuario Apellido',
            'usuario_login' => 'Usuario Login',
            'usuario_password' => 'Usuario Password',
            'usuario_fecha_creacion' => 'Usuario Fecha Creacion',
            'usuario_fecha_modificacion' => 'Usuario Fecha Modificacion',
            'usuario_fecha_ultimo_acceso' => 'Usuario Fecha Ultimo Acceso',
            'usuario_email' => 'Usuario Email',
            'usuario_telefono_1' => 'Usuario Telefono 1',
            'usuario_telefono_2' => 'Usuario Telefono 2',
            'usuario_direccion' => 'Usuario Direccion',
            'usuario_activo' => 'Usuario Activo',
            'usuario_online' => 'Usuario Online',
            'usuario_perfil' => 'Usuario Perfil',
            'usuario_imagen_1' => 'Usuario Imagen 1',
            'usuario_estado' => 'Usuario Estado',
            'usuario_mensaje_publico' => 'Usuario Mensaje Publico',
            'usuario_genero' => 'Usuario Genero',
            'usuario_fecha_nacimiento' => 'Usuario Fecha Nacimiento',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'usuario_fecha_codigo' => 'Usuario Fecha Codigo',
            'usuario_codigo' => 'Usuario Codigo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmsBanners()
    {
        return $this->hasMany(CmsBanners::className(), ['usuario_id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmsGeneralPages()
    {
        return $this->hasMany(CmsGeneralPages::className(), ['usuario_id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['usuario_id' => 'usuario_id']);
    }

    /**
     * @inheritdoc
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }
}
