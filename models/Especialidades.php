<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "especialidades".
 *
 * @property integer $especialidad_id
 * @property string $especialidad_nombre
 *
 * @property VideoEspecialidades[] $videoEspecialidades
 */
class Especialidades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'especialidades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['especialidad_nombre'], 'required'],
            [['especialidad_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'especialidad_id' => Yii::t('app', 'Especialidad ID'),
            'especialidad_nombre' => Yii::t('app', 'Especialidad Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideoEspecialidades()
    {
        return $this->hasMany(VideoEspecialidades::className(), ['especialidad_id' => 'especialidad_id']);
    }
}
