<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Especialistavideos;

/**
 * EspecialistavideosSearch represents the model behind the search form about `app\models\Especialistavideos`.
 */
class EspecialistavideosSearch extends Especialistavideos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['especialista_video_id', 'video_id', 'especialista_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Especialistavideos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'especialista_video_id' => $this->especialista_video_id,
            'video_id' => $this->video_id,
            'especialista_id' => $this->especialista_id,
        ]);

        return $dataProvider;
    }
}
