<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Videoespecialidades;

/**
 * VideoespecialidadesSearch represents the model behind the search form about `app\models\Videoespecialidades`.
 */
class VideoespecialidadesSearch extends Videoespecialidades
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_especialidad_id', 'especialidad_id', 'video_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Videoespecialidades::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'video_especialidad_id' => $this->video_especialidad_id,
            'especialidad_id' => $this->especialidad_id,
            'video_id' => $this->video_id,
        ]);

        return $dataProvider;
    }
}
