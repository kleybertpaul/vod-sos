<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuarios;

/**
 * UsuariosSearch represents the model behind the search form about `app\models\Usuarios`.
 */
class UsuariosSearch extends Usuarios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'pais_id'], 'integer'],
            [['usuario_nombre', 'usuario_apellido', 'usuario_clave', 'usuario_correo', 'usuario_fecha_de_nacimiento', 'usuario_grado_de_instruccion', 'usuario_perfil'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'usuario_id' => $this->usuario_id,
            'usuario_fecha_de_nacimiento' => $this->usuario_fecha_de_nacimiento,
            'usuario_grado_de_instruccion' => $this->usuario_grado_de_instruccion,
            'pais_id' => $this->pais_id,
        ]);

        $query->andFilterWhere(['like', 'usuario_nombre', $this->usuario_nombre])
            ->andFilterWhere(['like', 'usuario_apellido', $this->usuario_apellido])
            ->andFilterWhere(['like', 'usuario_clave', $this->usuario_clave])
            ->andFilterWhere(['like', 'usuario_correo', $this->usuario_correo])
            ->andFilterWhere(['like', 'usuario_perfil', $this->usuario_perfil]);

        return $dataProvider;
    }
}
