<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "especialistavideos".
 *
 * @property integer $especialista_video_id
 * @property integer $video_id
 * @property integer $especialista_id
 *
 * @property Especialistas $especialista
 * @property Videos $video
 */
class Especialistavideos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'especialistavideos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['especialista_video_id', 'video_id', 'especialista_id'], 'required'],
            [['especialista_video_id', 'video_id', 'especialista_id'], 'integer'],
            [['especialista_id'], 'exist', 'skipOnError' => true, 'targetClass' => Especialistas::className(), 'targetAttribute' => ['especialista_id' => 'especialista_id']],
            [['video_id'], 'exist', 'skipOnError' => true, 'targetClass' => Videos::className(), 'targetAttribute' => ['video_id' => 'video_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'especialista_video_id' => Yii::t('app', 'Especialista Video ID'),
            'video_id' => Yii::t('app', 'Video ID'),
            'especialista_id' => Yii::t('app', 'Especialista ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialista()
    {
        return $this->hasOne(Especialistas::className(), ['especialista_id' => 'especialista_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Videos::className(), ['video_id' => 'video_id']);
    }
}
