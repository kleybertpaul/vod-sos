<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pruebas;

/**
 * PruebasSearch represents the model behind the search form about `app\models\Pruebas`.
 */
class PruebasSearch extends Pruebas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prueba_id'], 'integer'],
            [['prueba_nombre', 'prueba_apellido'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pruebas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prueba_id' => $this->prueba_id,
        ]);

        $query->andFilterWhere(['like', 'prueba_nombre', $this->prueba_nombre])
            ->andFilterWhere(['like', 'prueba_apellido', $this->prueba_apellido]);

        return $dataProvider;
    }
}
