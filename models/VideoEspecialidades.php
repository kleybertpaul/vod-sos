<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "videoespecialidades".
 *
 * @property integer $video_especialidad_id
 * @property integer $especialidad_id
 * @property integer $video_id
 *
 * @property Especialidades $especialidad
 * @property Videos $video
 */
class Videoespecialidades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'videoespecialidades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['especialidad_id', 'video_id'], 'required'],
            [['especialidad_id', 'video_id'], 'integer'],
            [['especialidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Especialidades::className(), 'targetAttribute' => ['especialidad_id' => 'especialidad_id']],
            [['video_id'], 'exist', 'skipOnError' => true, 'targetClass' => Videos::className(), 'targetAttribute' => ['video_id' => 'video_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'video_especialidad_id' => Yii::t('app', 'Video Especialidad ID'),
            'especialidad_id' => Yii::t('app', 'Especialidad ID'),
            'video_id' => Yii::t('app', 'Video ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialidad()
    {
        return $this->hasOne(Especialidades::className(), ['especialidad_id' => 'especialidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Videos::className(), ['video_id' => 'video_id']);
    }
}
