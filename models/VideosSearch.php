<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Videos;

/**
 * VideosSearch represents the model behind the search form about `app\models\Videos`.
 */
class VideosSearch extends Videos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'video_cantidad_reproducciones', 'video_activo', 'especialista_id', 'video_favorito', 'usuario_id'], 'integer'],
            [['video_nombre_archivo', 'video_titulo', 'video_duracion', 'video_formato', 'video_descripcion', 'video_palabras_claves', 'video_tipo', 'video_fecha_creacion', 'video_fecha_actualizacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Videos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'video_id' => $this->video_id,
            'video_cantidad_reproducciones' => $this->video_cantidad_reproducciones,
            'video_activo' => $this->video_activo,
            'especialista_id' => $this->especialista_id,
            'video_favorito' => $this->video_favorito,
            'video_fecha_creacion' => $this->video_fecha_creacion,
            'video_fecha_actualizacion' => $this->video_fecha_actualizacion,
            'usuario_id' => $this->usuario_id,
        ]);

        $query->andFilterWhere(['like', 'video_nombre_archivo', $this->video_nombre_archivo])
            ->andFilterWhere(['like', 'video_titulo', $this->video_titulo])
            ->andFilterWhere(['like', 'video_duracion', $this->video_duracion])
            ->andFilterWhere(['like', 'video_formato', $this->video_formato])
            ->andFilterWhere(['like', 'video_descripcion', $this->video_descripcion])
            ->andFilterWhere(['like', 'video_palabras_claves', $this->video_palabras_claves])
            ->andFilterWhere(['like', 'video_tipo', $this->video_tipo]);

        return $dataProvider;
    }
}
