<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Listasreproduccion;

/**
 * ListasreproduccionSearch represents the model behind the search form about `app\models\Listasreproduccion`.
 */
class ListasreproduccionSearch extends Listasreproduccion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lista_reproduccion_id', 'usuario_id'], 'integer'],
            [['lista_reproduccion_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Listasreproduccion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lista_reproduccion_id' => $this->lista_reproduccion_id,
            'usuario_id' => $this->usuario_id,
        ]);

        $query->andFilterWhere(['like', 'lista_reproduccion_nombre', $this->lista_reproduccion_nombre]);

        return $dataProvider;
    }
}
