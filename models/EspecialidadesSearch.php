<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Especialidades;

/**
 * EspecialidadesSearch represents the model behind the search form about `app\models\Especialidades`.
 */
class EspecialidadesSearch extends Especialidades
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['especialidad_id'], 'integer'],
            [['especialidad_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Especialidades::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'especialidad_id' => $this->especialidad_id,
        ]);

        $query->andFilterWhere(['like', 'especialidad_nombre', $this->especialidad_nombre]);

        return $dataProvider;
    }
}
