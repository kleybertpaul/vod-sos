<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paises".
 *
 * @property integer $pais_id
 * @property string $pais_nombre
 *
 * @property Usuarios[] $usuarios
 */
class Paises extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paises';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pais_nombre'], 'required'],
            [['pais_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pais_id' => Yii::t('app', 'Pais ID'),
            'pais_nombre' => Yii::t('app', 'Pais Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['pais_id' => 'pais_id']);
    }
}
