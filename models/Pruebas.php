<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pruebas".
 *
 * @property integer $prueba_id
 * @property string $prueba_nombre
 * @property string $prueba_apellido
 */
class Pruebas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pruebas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prueba_nombre'], 'required'],
            [['prueba_nombre', 'prueba_apellido'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prueba_id' => Yii::t('app', 'Prueba ID'),
            'prueba_nombre' => Yii::t('app', 'Prueba Nombre'),
            'prueba_apellido' => Yii::t('app', 'Prueba Apellido'),
        ];
    }
}
