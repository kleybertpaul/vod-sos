<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "especialistas".
 *
 * @property integer $especialista_id
 * @property string $especialista_nombre
 *
 * @property EspecialistaVideos[] $especialistaVideos
 * @property Videos[] $videos
 */
class Especialistas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'especialistas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['especialista_nombre'], 'required'],
            [['especialista_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'especialista_id' => Yii::t('app', 'Especialista ID'),
            'especialista_nombre' => Yii::t('app', 'Especialista Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialistaVideos()
    {
        return $this->hasMany(EspecialistaVideos::className(), ['especialista_id' => 'especialista_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(Videos::className(), ['especialista_id' => 'especialista_id']);
    }
}
