<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "listareproduccionvideos".
 *
 * @property integer $lista_reproduccion_video_id
 * @property integer $video_id
 * @property integer $lista_reproduccion_id
 *
 * @property Listasreproduccion $listaReproduccion
 * @property Videos $video
 */
class Listareproduccionvideos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'listareproduccionvideos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lista_reproduccion_video_id', 'video_id', 'lista_reproduccion_id'], 'required'],
            [['lista_reproduccion_video_id', 'video_id', 'lista_reproduccion_id'], 'integer'],
            [['lista_reproduccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Listasreproduccion::className(), 'targetAttribute' => ['lista_reproduccion_id' => 'lista_reproduccion_id']],
            [['video_id'], 'exist', 'skipOnError' => true, 'targetClass' => Videos::className(), 'targetAttribute' => ['video_id' => 'video_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lista_reproduccion_video_id' => Yii::t('app', 'Lista Reproduccion Video ID'),
            'video_id' => Yii::t('app', 'Video ID'),
            'lista_reproduccion_id' => Yii::t('app', 'Lista Reproduccion ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListaReproduccion()
    {
        return $this->hasOne(Listasreproduccion::className(), ['lista_reproduccion_id' => 'lista_reproduccion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Videos::className(), ['video_id' => 'video_id']);
    }
}
