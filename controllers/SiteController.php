<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\helpers\html;
use yii\db\Expression;
use app\models\Paises;
use app\models\Usuarios;

class SiteController extends Controller
{

    

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex() {


                     return $this->redirect('index');


    }


    public function actionLogin() {


        $model = new LoginForm();
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                  if (Yii::$app->user->identity->usuario_activo =="1"){
                    if (Yii::$app->user->identity->usuario_perfil == "administrador") {
                     return $this->redirect(['/users/update/','id'=>Yii::$app->user->identity->usuario_id]);
                    } else{

                      }
                  } else{
                        Yii::$app->session->setFlash('exception', "<span class='fa fa-exclamation-triangle '></span> Usuario Bloqueado");

                        return $this->redirect(['/login']);
                    }
                }

                return $this->render('login', [
                    'model' => $model,
                ]);
    }


    public function actionLogout() {

      Yii::$app->user->logout();
      return $this->goHome();

    }



public function actionUrlmanage($http) {


$this->layout= "sostelemedicinavod";

        switch($http){

          case 'FormularioRegistro':
      
  $modelcorreo  =  Usuarios::find()->where(['usuario_correo' =>$_POST['email']])->all();
  if($modelcorreo) {
  Yii::$app->session->setFlash("error", "El correo ya existe" );
  return $this->redirect(['/registrousuario']);  } 

  
  if($_POST['password'] != $_POST['confirmacionpassword']){
  Yii::$app->session->setFlash("error", "Las claves no son iguales" );
  return $this->redirect(['/registrousuario']); }
  


  if($_POST['email'] !=$_POST['confirmacionemail'] ){
  Yii::$app->session->setFlash("error", "Los Correos no son iguales" );
  return $this->redirect(['/registrousuario']); }
    
    else {
    $model = new Usuarios();
    $model->usuario_nombre = $_POST['name'];
    $model->usuario_apellido = $_POST['apellido'];
    $model->usuario_clave = $_POST['password'];
    $model->usuario_correo = $_POST['email'];
    $model->usuario_fecha_de_nacimiento = $_POST['fechanacimiento'];
    $model->pais_id = $_POST['pais'];
  
      if ($model->save(false)){
      $mensajeguardado = \Yii::t('app', 'Guardado');
      Yii::$app->session->setFlash("success",$mensajeguardado);
      return $this->redirect(['/index']); 
      }
    else{
        $mensajeerror=\Yii::t('app', 'ha ocurrido un error');
        (Yii::$app->session->hasFlash("error", $mensajeerror));
   //getErrors();
    }
}
    
          break;

                case 'logout':

                  Yii::$app->user->logout();
                  return $this->goHome();

                break;

                  case 'index':

                  return $this->render('index');

                  break;

                  case 'vervideo':

                  return $this->render('vervideo');

                  break;

                  case 'registrousuario':

                  $modelpais  =  Paises::find()->all();




                  return $this->render('registrousuario',['modelpais' =>$modelpais]);

                    // code...
                    break;

                    case 'register':
                    return $this->render('register');
                      // code...
                      break;





                  case 'login':

                  $model = new LoginForm();
                    if ($model->load(Yii::$app->request->post()) && $model->login()) {
                      if (Yii::$app->user->identity->usuario_activo =="1"){
                        if (Yii::$app->user->identity->usuario_perfil == "administrador") {
                         return $this->redirect(['/users/update/','id'=>Yii::$app->user->identity->usuario_id]);

                        } else{

                          //return $this->render('account', ['lang' =>$lang]);
                          return $this->redirect(['/account?lang='.$_GET['lang']]);

                          }
                      } else{
                            Yii::$app->session->setFlash('exception', "<span class='fa fa-exclamation-triangle '></span> Usuario Bloqueado");

                            return $this->redirect(['/login']);
                        }
                    }

                    return $this->render('login', [
                        'model' => $model,
                    ]);

                    break;









                }
              }
            }



    ?>
