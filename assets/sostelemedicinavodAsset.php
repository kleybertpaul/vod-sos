<?php

namespace app\assets;

use yii\web\AssetBundle;

class sostelemedicinavodAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = array( 'position' => \yii\web\View::POS_HEAD );

    public $css = [


            'themes/sostelemedicinavod/css/mobirise-icons/mobirise-icons.css',
            'themes/sostelemedicinavod/css/tether/tether.min.css',
            'themes/sostelemedicinavod/css/bootstrap.min.css',
            'themes/sostelemedicinavod/css/bootstrap-grid.min.css',
            'themes/sostelemedicinavod/css/bootstrap-reboot.min.css',
            'themes/sostelemedicinavod/css/dropdown/css/style.css',
            'themes/sostelemedicinavod/css/socicon/css/styles.css',
            'themes/sostelemedicinavod/css/theme/css/style.css',
            'themes/sostelemedicinavod/css/gallery/style.css',
            'themes/sostelemedicinavod/css/mbr-additional.css',



      ];

    public $js = [

               
               'themes/sostelemedicinavod/js/jquery/jquery.min.js',
               'themes/sostelemedicinavod/js/popper/popper.min.js',
               'themes/sostelemedicinavod/js/tether/tether.min.js',
               'themes/sostelemedicinavod/js/bootstrap.min.js',
               'themes/sostelemedicinavod/js/smooth-scroll.js',
               'themes/sostelemedicinavod/js/script.min.js',
               'themes/sostelemedicinavod/js/jquery/jquery.touch-swipe.min.js',
               'themes/sostelemedicinavod/js/jquery/jquery.mb.vimeo_player.js',
               'themes/sostelemedicinavod/js/masonry/masonry.pkgd.min.js',
               'themes/sostelemedicinavod/js/imagesloaded/imagesloaded.pkgd.min.js',
               'themes/sostelemedicinavod/js/bootstrap-carousel-swipe.js',
               'themes/sostelemedicinavod/js/jquery/jquery.mb.ytplayer.min.js',
               'themes/sostelemedicinavod/js/theme/js/script.js',
               'themes/sostelemedicinavod/js/slidervideo/script.js',
               'themes/sostelemedicinavod/js/gallery/player.min.js',
               'themes/sostelemedicinavod/js/gallery/script.js',

    ];
    public $depends = [
       // 'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
